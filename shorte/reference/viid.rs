use curl::easy::{Easy2, List, WriteError};
use curl::easy::Handler;

use select::document::Document;
use select::predicate::Name;

use ratel::parser;
use ratel::grammar::Statement::VariableDeclaration;

use std::time::{Duration, Instant};
use std::thread;


fn isolate_sess(body: &str) -> ApiResult<String>{
	println!("Body Len: {}", body.len());
	//let document = Document::from(body);
	/*let mut script = document
		.find(Name("script"))
		.filter(|el| el.attr("type").is_some())
		.nth(2)
		.ok_or(ApiError::BodyParse(body.to_string()))?
		.text();*/
	//println!("{}", &script);
	let script = body;
	let begin_locator = "sessionId: \"";
	let begin = script.find(begin_locator).expect("Could not find beginner index") + begin_locator.len();
	let begin_slice = &script[begin..];
	let end = begin_slice.find("\",").expect("Could not find close");
	let sess = begin_slice[0..end].to_string();
	return Ok(sess);
}

pub struct Api {
	handle: Easy2<BufferBody>,
}

impl Api {
	pub fn new() -> Self{
		let mut handle = Easy2::new(BufferBody::new());
		handle.useragent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3620.0 Safari/537.36").unwrap();
		Api {
			handle
		}
	}
	
	fn begin_work(&mut self, url: &str) -> ApiResult<Work> {
		let mut work = Work::from_url(url)?;
		let challenge_url = work.get_challenge_url();
		println!("Challenge Url: {}", &challenge_url);
		self.handle.referer(&challenge_url).unwrap();
		self.handle.url(&challenge_url).unwrap();
		self.handle.perform().unwrap();
		println!("Code: {}", self.handle.response_code().unwrap());
		work.start();
		
		let raw = self.handle.get_mut().take_buffer();
		let body = String::from_utf8(raw).unwrap();
		let sess = isolate_sess(&body)?;
		work.set_sess(&sess);
		println!("Sess: {}", &sess);
		
		return Ok(work);
	}
	
	fn finish_work(&mut self, work: &Work) -> ApiResult<String>{
		self.handle.useragent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3620.0 Safari/537.36").unwrap();
		let api_url = work.get_api_url();
		println!("{}", &api_url);
		self.handle.url(&api_url).unwrap();
		self.handle.referer(&work.link).unwrap();
		self.handle.perform().unwrap();
		let raw = self.handle.get_mut().take_buffer();
		let body = String::from_utf8(raw).unwrap().replace("\\/", "/");
		return Ok(body);
	}
	
	pub fn resolve(&mut self, url: &str) -> ApiResult<String>{
		let work = self.begin_work(url)?;
		thread::sleep(work.get_sleep().unwrap());
		let link = self.finish_work(&work);
		return link;
	}
	
	pub fn resolve_many(&mut self, urls: &[String]) -> ApiResult<Vec<String>>{
		let work_vec: Vec<Work> = urls
			.iter()
			.map(|el| self.begin_work(el).unwrap())
			.collect();
			
		thread::sleep(work_vec[0].get_sleep().unwrap());
		let ret: Vec<String> = work_vec
			.iter()
			.map(|work| self.finish_work(work).unwrap())
			.collect();
		return Ok(ret);
	}
}

pub type ApiResult<T> = Result<T, ApiError>;

const IP: &'static str = "78.140.188.187";

struct Work {
	link: String,
	path: String,
	sess: String,
	start: Option<Instant>
}

impl Work {
	pub fn from_url(link: &str) -> ApiResult<Self>{
		let url = url::Url::parse(link).expect("Error Parsing URLs"); //Error?
		//let host = url.host_str().unwrap(); //Error?
		return Ok(Work {
			link: link.to_string(),
			path: url.path().to_string(),
			sess: String::new(),
			start: None,
		});
	}
	
	pub fn get_challenge_url(&self) -> String{
		let url = format!("http://{}{}", IP, &self.path);
		return url;
	}
	
	pub fn get_api_url(&self) -> String{
		return format!("http://{}/shortest-url/end-adsession?adSessionId={}&adbd=0", IP, &self.sess);
	}
	
	pub fn set_sess(&mut self, sess: &str){
		self.sess = sess.to_string();
	}
	
	pub fn start(&mut self) {
		self.start = Some(Instant::now());
	}
	
	pub fn get_sleep(&self) -> Option<Duration>{
		if let Some(ref start) = self.start {
			return Some(Duration::new(4, 0) - start.elapsed());
		}
		
		return None;
	}
}

#[derive(Debug)]
pub enum ApiError{
	BodyParse(String),
	ScriptParse(String),
}

struct BufferBody {
	buffer: Vec<u8>
}

impl BufferBody {
	pub fn new() -> Self {
		return BufferBody {
			buffer: Vec::new(),
		}
	}
	
	pub fn take_buffer(&mut self) -> Vec<u8>{
		let mut buffer = Vec::new();
		std::mem::swap(&mut buffer, &mut self.buffer);
		return buffer;
	}
	
	pub fn reset(&mut self){
		self.buffer.truncate(0);
	}
}

impl Handler for BufferBody {
    fn write(&mut self, data: &[u8]) -> Result<usize, WriteError> {
        self.buffer.extend_from_slice(data);
        Ok(data.len())
    }
}