use shorte::{
    Client,
    ShorteUrl,
    Url,
};

async fn test_resolve(url: &str) {
    let client = Client::new();
    let url = Url::parse(url).expect("Valid Url");
    let url = ShorteUrl::from_url(url).expect("Valid Shorte Url");
    client.resolve(&url).await.expect("Good resolve");
}

#[tokio::test]
async fn festyy() {
    test_resolve("https://festyy.com/wuwDL7").await;
}

#[tokio::test]
async fn cllkme() {
    test_resolve("https://cllkme.com/wuwDL7").await;
}

#[tokio::test]
async fn gestyy() {
    test_resolve("http://gestyy.com/wMlspw").await;
}

#[test]
fn invalid_host() {
    let url = Url::parse("https://postimg.org/image/ajfd5oygf/").unwrap();
    let url_res = ShorteUrl::from_url(url);
    assert!(url_res.is_err());
}

#[test]
fn bad_short_path() {
    let url = Url::parse("https://cllkme.com/").unwrap();
    let url_res = ShorteUrl::from_url(url);
    assert!(url_res.is_err());
}
