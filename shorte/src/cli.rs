use crate::ShorteUrl;
use clap::{
    App,
    Arg,
    ArgMatches,
};
use url::Url;

pub fn cli() -> App<'static, 'static> {
    App::new("shorte").arg(Arg::with_name("url").required(true))
}

pub fn exec(matches: &ArgMatches) {
    let url = matches.value_of("url").unwrap();

    let client = crate::Client::new();

    let url = match Url::parse(url) {
        Ok(u) => u,
        Err(e) => {
            println!("Unable to parse url, got error: {:#?}", e);
            return;
        }
    };

    let url = match ShorteUrl::from_url(url) {
        Ok(u) => u,
        Err(e) => {
            println!("Non shorte url, got error: {:#?}", e);
            return;
        }
    };

    let mut rt = match tokio::runtime::Runtime::new() {
        Ok(rt) => rt,
        Err(e) => {
            println!("Failed to init runtime, got: {:#?}", e);
            return;
        }
    };

    let link = match rt.block_on(client.resolve(&url)) {
        Ok(l) => l,
        Err(e) => {
            println!("Failed to resolve '{}', got error: {:#?}", url.as_url(), e);
            return;
        }
    };

    println!("{}", link);
}
