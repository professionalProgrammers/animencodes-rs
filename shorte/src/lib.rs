#[cfg(feature = "cli")]
pub mod cli;
mod client;
mod error;
mod shorte_url;

pub use crate::{
    error::{
        Error,
        ShorteResult,
    },
    shorte_url::ShorteUrl,
};
pub use client::Client;
pub use url::Url;
