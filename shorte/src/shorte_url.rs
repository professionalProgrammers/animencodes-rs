use url::Url;

const VALID_HOSTS: &[&str] = &[
    "ceesty.com",
    "cllkme.com",
    "corneey.com",
    "destyy.com",
    "festyy.com",
    "gestyy.com",
    "shorte.st",
    "viid.me",
];

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct ShorteUrl(Url);

impl ShorteUrl {
    pub fn as_url(&self) -> &Url {
        &self.0
    }

    pub fn code(&self) -> &str {
        &self.0.path()[1..]
    }

    pub fn from_url_unchecked(url: Url) -> Self {
        ShorteUrl(url)
    }

    pub fn verify_host(host: &str) -> Result<(), HostError> {
        if !VALID_HOSTS.contains(&host) {
            return Err(HostError(host.to_string()));
        }

        Ok(())
    }

    pub fn verify_path(path: &str) -> Result<(), PathError> {
        let len = path.len();
        if !(2..=7).contains(&len) {
            //TODO: Find exact bounds
            return Err(PathError::InvalidLen(len));
        }

        for (i, c) in path.chars().skip(1).enumerate() {
            if !c.is_ascii_alphanumeric() {
                return Err(PathError::InvalidChar(i, c));
            }
        }

        Ok(())
    }

    pub fn from_url(url: Url) -> Result<Self, FromUrlError> {
        let host = url.host_str().ok_or(FromUrlError::MissingHost)?;
        Self::verify_host(host).map_err(FromUrlError::InvalidHost)?;
        Self::verify_path(url.path()).map_err(FromUrlError::InvalidPath)?;
        Ok(Self(url))
    }

    pub fn into_url(self) -> Url {
        self.0
    }
}

#[derive(Debug)]
pub struct HostError(String);

#[derive(Debug)]
pub enum PathError {
    InvalidLen(usize),
    InvalidChar(usize, char),
}

#[derive(Debug)]
pub enum FromUrlError {
    MissingHost,
    InvalidHost(HostError),
    InvalidPath(PathError),
}
