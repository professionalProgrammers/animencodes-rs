use crate::{
    Error,
    ShorteResult,
    ShorteUrl,
};
use bytes::buf::BufExt;
use futures::stream::StreamExt;
use reqwest::{
    header::{
        HeaderMap,
        HeaderValue,
        LOCATION,
        USER_AGENT,
    },
    redirect::Policy,
    StatusCode,
};
use select::{
    document::Document,
    predicate::Name,
};
use std::borrow::Borrow;
use url::Url;

const USER_AGENT_STR: &str =
    "User-Agent': 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)";
const IP: &str = "http://78.140.188.187";

#[derive(Default)]
pub struct Client {
    client: reqwest::Client,
}

impl Client {
    pub fn new() -> Self {
        let mut headers = HeaderMap::new();
        headers.insert(USER_AGENT, HeaderValue::from_static(USER_AGENT_STR));

        let client = reqwest::Client::builder()
            .default_headers(headers)
            .redirect(Policy::none())
            .build()
            .unwrap();

        Client { client }
    }

    pub async fn make_shorte_url(&self, url: &Url) -> ShorteResult<Option<ShorteUrl>> {
        if let Ok(u) = ShorteUrl::from_url(url.clone()) {
            return Ok(Some(u));
        }

        let mut no_path_url = url.clone();
        no_path_url.set_path("");

        let res = self.client.get(no_path_url.as_str()).send().await;

        match res {
            Ok(res) => {
                let valid_redirect = res
                    .headers()
                    .get(LOCATION)
                    .and_then(|loc| Url::parse(loc.to_str().ok()?).ok())
                    .and_then(|url| Some(url.host_str()? == "shorte.st"))
                    .unwrap_or(false);

                if res.status() == StatusCode::MOVED_PERMANENTLY && valid_redirect {
                    return Ok(Some(ShorteUrl::from_url_unchecked(url.clone())));
                }
            }
            Err(_e) => (),
        }

        Ok(None)
    }

    pub async fn resolve(&self, url: &ShorteUrl) -> ShorteResult<String> {
        let ip_url = Url::parse(IP).unwrap().join(url.code()).unwrap();
        let res = self.client.get(ip_url).send().await?;

        Document::from_read(res.bytes().await?.reader())?
            .find(Name("a"))
            .filter_map(|el| el.attr("href"))
            .last()
            .map(|el| el.to_string())
            .ok_or(Error::MissingRedirect)
    }

    pub async fn resolve_many_par<I: Iterator<Item = T>, T: Borrow<ShorteUrl>>(
        &self,
        iter: I,
    ) -> Vec<ShorteResult<String>> {
        futures::future::join_all(iter.map(|url| async move { self.resolve(url.borrow()).await }))
            .await
    }

    pub async fn resolve_many_seq<I: Iterator<Item = T>, T: Borrow<ShorteUrl>>(
        &self,
        iter: I,
    ) -> Vec<ShorteResult<String>> {
        futures::stream::iter(iter)
            .then(|url| async move { self.resolve(url.borrow()).await })
            .collect()
            .await
    }
}
