// Breaks on CI for some reason...
#[ignore]
#[tokio::test]
async fn it_works_short() {
    let client = ouo::Client::new();
    let url = ouo::OuoUrl::from_url_str("https://ouo.io/4taT4").unwrap();
    let u = client.resolve(&url).await.unwrap();
    assert_eq!(u.as_str(), "http://www.google.com/");
}

#[ignore]
#[tokio::test]
async fn it_works_long() {
    let client = ouo::Client::new();
    let url = ouo::OuoUrl::from_url_str("https://ouo.io/1hyPPN").unwrap();
    let u = client.resolve(&url).await.unwrap();
    assert_eq!(
        u.as_str(),
        "https://mega.nz/#!RK5W2DgT!u0ZBxtyWGJ4LtiuCR97eE2eORQUJG0JkEyZMfxkhhsw"
    );
}
