use crate::{
    Error,
    OuoUrl,
};
use futures::StreamExt;
use select::{
    document::Document,
    predicate::{
        And,
        Attr,
        Name,
    },
};
use std::borrow::Borrow;
use url::Url;

pub struct Client {
    client: reqwest::Client,
}

impl Client {
    pub fn new() -> Self {
        Client {
            client: reqwest::Client::builder()
                .redirect(reqwest::redirect::Policy::none())
                .build()
                .unwrap(),
        }
    }

    pub async fn resolve(&self, url: &OuoUrl) -> Result<Url, Error> {
        let res = {
            let res = self
                .client
                .get(&format!("https://ouo.io/fbc/{}", url.code()))
                .send()
                .await?;

            let status = res.status();
            if !status.is_success() {
                return Err(Error::InvalidStatus(status));
            }

            res
        };

        let session_cookie = res
            .cookies()
            .find(|c| c.name() == "ouoio_session")
            .map(|cookie| format!("{}={}", cookie.name(), cookie.value()))
            .ok_or(Error::MissingSessionCookie)?;

        let (token, v_token) = {
            let doc = Document::from(res.text().await?.as_str());
            let token = doc
                .find(And(Name("input"), Attr("name", "_token")))
                .last()
                .ok_or(Error::MissingToken)?
                .attr("value")
                .ok_or(Error::MissingToken)?
                .to_string();

            let v_token = doc
                .find(And(Name("input"), Attr("name", "v-token")))
                .last()
                .ok_or(Error::MissingVToken)?
                .attr("value")
                .ok_or(Error::MissingVToken)?
                .to_string();

            (token, v_token)
        };

        let res = {
            let res = self
                .client
                .post(&format!("https://ouo.io/go/{}", url.code()))
                .header(reqwest::header::COOKIE, &session_cookie)
                .form(&[("_token", token), ("v-token", v_token)])
                .send()
                .await?;

            let status = res.status();
            if !status.is_success() {
                if status != reqwest::StatusCode::FOUND {
                    return Err(Error::InvalidStatus(status));
                }

                let url = res
                    .headers()
                    .get(reqwest::header::LOCATION)
                    .ok_or(Error::MissingRedirectUrl)?
                    .to_str()
                    .map_err(|_| Error::MissingRedirectUrl)?;

                let url = Url::parse(url).map_err(Error::RedirectUrlParse)?;

                if url.host_str() == Some("ouo.io") {
                    return Err(Error::InvalidRedirect(url));
                } else {
                    return Ok(url);
                }
            }

            res
        };

        let token = {
            let doc = Document::from(res.text().await?.as_str());
            doc.find(And(Name("input"), Attr("name", "_token")))
                .last()
                .ok_or(Error::MissingToken)?
                .attr("value")
                .ok_or(Error::MissingToken)?
                .to_string()
        };

        let res = self
            .client
            .post(&format!("https://ouo.io/xreallcygo/{}", url.code()))
            .header(reqwest::header::COOKIE, &session_cookie)
            .form(&[("_token", token), ("x-token", String::new())])
            .send()
            .await?;

        let status = res.status();
        if status != reqwest::StatusCode::FOUND {
            return Err(Error::InvalidStatus(status));
        }

        let url = res
            .headers()
            .get(reqwest::header::LOCATION)
            .ok_or(Error::MissingRedirectUrl)?
            .to_str()
            .map_err(|_| Error::MissingRedirectUrl)?;

        let url = Url::parse(url).map_err(Error::RedirectUrlParse)?;

        if url.host_str() == Some("ouo.io") {
            Err(Error::InvalidRedirect(url))
        } else {
            Ok(url)
        }
    }

    pub async fn resolve_many_par<I: Iterator<Item = T>, T: Borrow<OuoUrl>>(
        &self,
        iter: I,
    ) -> Vec<Result<Url, Error>> {
        futures::future::join_all(iter.map(|url| async move { self.resolve(url.borrow()).await }))
            .await
    }

    pub async fn resolve_many_seq<I: Iterator<Item = T>, T: Borrow<OuoUrl>>(
        &self,
        iter: I,
    ) -> Vec<Result<Url, Error>> {
        futures::stream::iter(iter)
            .then(|url| async move { self.resolve(url.borrow()).await })
            .collect()
            .await
    }
}

impl Default for Client {
    fn default() -> Self {
        Client::new()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    fn assert_send<T: Send>(_t: T) {}

    #[test]
    fn static_assertions() {
        let url = OuoUrl::from_url_str("https://ouo.io/4taT4").unwrap();
        let client = Client::new();
        let resolve = client.resolve(&url);

        assert_send(resolve);
        assert_send(client);
    }
}
