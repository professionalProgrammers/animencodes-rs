use url::Url;

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct OuoUrl(Url);

impl OuoUrl {
    pub fn as_url(&self) -> &Url {
        &self.0
    }

    pub fn code(&self) -> &str {
        &self.0.path()[1..]
    }

    pub fn verify_host(host: &str) -> Result<(), HostError> {
        if host != "ouo.io" {
            return Err(HostError(host.to_string()));
        }

        Ok(())
    }

    pub fn verify_path(path: &str) -> Result<(), PathError> {
        let len = path.len();
        if !(4..=10).contains(&len) {
            // TODO: Find bounds
            return Err(PathError::InvalidLen(len));
        }

        for (i, c) in path.chars().skip(1).enumerate() {
            if !c.is_ascii_alphanumeric() {
                return Err(PathError::InvalidChar(i, c));
            }
        }

        Ok(())
    }

    pub fn verify_url(url: &Url) -> Result<(), FromUrlError> {
        let host_str = url.host_str().ok_or(FromUrlError::MissingHost)?;
        Self::verify_host(host_str).map_err(FromUrlError::InvalidHost)?;
        Self::verify_path(url.path()).map_err(FromUrlError::InvalidPath)?;
        Ok(())
    }

    pub fn from_url(url: Url) -> Result<Self, FromUrlError> {
        Self::verify_url(&url)?;
        Ok(Self(url))
    }

    pub fn from_url_str(s: &str) -> Result<Self, FromUrlStrError> {
        let url = Url::parse(s).map_err(FromUrlStrError::Parse)?;
        Self::from_url(url).map_err(FromUrlStrError::FromUrl)
    }

    pub fn into_url(self) -> Url {
        self.0
    }
}

#[derive(Debug)]
pub struct HostError(String);

#[derive(Debug)]
pub enum PathError {
    InvalidLen(usize),
    InvalidChar(usize, char),
}

#[derive(Debug)]
pub enum FromUrlError {
    MissingHost,
    InvalidHost(HostError),
    InvalidPath(PathError),
}

#[derive(Debug)]
pub enum FromUrlStrError {
    Parse(url::ParseError),
    FromUrl(FromUrlError),
}
