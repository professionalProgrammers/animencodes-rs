use url::Url;

#[derive(Debug)]
pub enum Error {
    Reqwest(reqwest::Error),
    InvalidStatus(reqwest::StatusCode),

    MissingSessionCookie,

    MissingToken,
    MissingVToken,

    MissingRedirectUrl,
    RedirectUrlParse(url::ParseError),

    InvalidRedirect(Url),
}

impl From<reqwest::Error> for Error {
    fn from(e: reqwest::Error) -> Self {
        Error::Reqwest(e)
    }
}
