use animencodes::{
    AnimeEntry,
    AnimeType,
    Client,
    MainPage,
};
use lazy_static::lazy_static;
use parking_lot::{
    Mutex,
    MutexGuard,
};

lazy_static! {
    static ref MAIN_PAGE: Mutex<MainPage> = {
        let client = Client::new();
        let mut rt = tokio::runtime::Runtime::new().unwrap();
        let page = rt.block_on(client.get_main_page()).unwrap();
        Mutex::new(page)
    };
}

fn get_main_page<'a>() -> MutexGuard<'a, animencodes::MainPage> {
    MAIN_PAGE.lock()
}

#[test]
fn get_new_anime() {
    let page = get_main_page();
    let data = page.new_anime();
    assert!(!data.is_empty());
    println!("{:#?}", data);
}

#[test]
fn get_all_entries() {
    let page = get_main_page();
    let data1 = page.subbed_entries();
    let data2 = page.dubbed_entries();
    assert!(!data1.is_empty());
    assert!(data1.iter().all(|e| e.anime_type == Some(AnimeType::Sub)));
    assert!(!data2.is_empty());
    assert!(data2.iter().all(|e| e.anime_type == Some(AnimeType::Dub)));
}

#[test]
fn parse_new_anime_str() {
    let test_data = vec![
        "Darker than Black: Kuro no Keiyakusha [1080][Hevc][Dual]",
        "Hand Shakers [1080][Hevc][Dual]",
        "Persona 3 the Movies [1080][Hevc][Sub]",
        "Sousei no Onmyouji [1080][Hevc][Dual]",
        "ReLIFE: Kanketsu-hen [1080][Hevc][Dual]",
        "Lupin III: Bye Bye Liberty - Kiki Ippatsu! [1080][Hevc][Dual]",
        "Hataraku Saibou [1080][Hevc][Sub]",
        "Cross Ange: Tenshi to Ryuu no Rondo [1080][Hevc][Dual]",
        "Seiken no Blacksmith [1080][Hevc][Dual] | Denpa Onna to Seishun Otoko [1080][Hevc][Sub] | Shingetsutan Tsukihime [576][Hevc][Dual]",
        "K-On!, K-On!! [1080][Hevc][Dual]",
        "Overlord II, JoJo no Kimyou na Bouken: Stardust Crusaders 2nd Season [1080][Hevc][Dual] | Grand Blue [1080][Hevc][Sub]",
        "Bleach (064-083), Dragon Ball Super (66-78) [720/1080][Hevc][Dual] | Bleach Movies [1080][Hevc][Dual]",
        "Boku no Kanojo ga Majimesugiru Sho-bitch na Ken, Citrus, Love Live! School Idol Project 2nd Season [1080][Hevc][Dual]",
        "Tsuritama [1080][Hevc][Dual] | Joshiraku, Top wo Nerae! Gunbuster, Top wo Nerae 2! Diebuster [1080][Hevc][Sub]",
        "Ore no Nounai Sentakushi ga Gakuen Love Comedy wo Zenryoku de Jama Shiteiru, Seireitsukai no Blade Dance [1080][Hevc][Sub] | Mary to Majo no Hana [1080][Hevc][Dual]",
		"Boogiepop wa Warawanai (2000) [480][Hevc][Dual]",
		"Kanata no Astra [1080][Hevc][Dual] | Beastars [Hevc][Sub] | Ore wo Suki nano wa Omae dake ka yo [1080][Hevc][Sub]",
	];

    for name in &test_data {
        let data = AnimeEntry::from_new_anime_name(name).unwrap();
        assert!(!data.is_empty());
        println!("{:#?}", data);
    }
}

#[test]
fn parse_anime_str() {
    let test_data = vec!["[Cleo] C: The Money of Soul and Possibility Control | [C] CONTROL - The Money and Soul of Possibility [Dual Audio 10bit BD1080p][HEVC-x265]"];
    for name in test_data.iter() {
        let data = AnimeEntry::from_name(name).unwrap();
        println!("{:#?}", data);
    }
}

#[test]
fn encoders() {
    let data = get_main_page();
    data.subbed_entries()
        .iter()
        .chain(data.dubbed_entries())
        .for_each(|a| {
            if !a.encoder.as_ref().unwrap().is_known() {
                panic!("Unknown Encoder: {:#?}", a.encoder);
            }
        });
}

#[test]
fn links() {
    let data = get_main_page();
    for e in data.subbed_entries().iter().chain(data.dubbed_entries()) {
        if let Some(link) = e.link.as_ref() {
            assert!(!link.is_unknown(), "{:#?}", link);
        } else {
            panic!("Missing link");
        }
    }
}
