#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash, serde::Serialize, serde::Deserialize)]
pub enum Encoding {
    H265,
    H264,
}
