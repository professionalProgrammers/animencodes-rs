#[derive(Debug, Eq, PartialEq, Clone, serde::Serialize, serde::Deserialize)]
#[serde(untagged)]
pub enum Resolution {
    Single(u32),
    Multiple(Vec<u32>), // Don't use resolution here to prevent nesting.
}

impl std::fmt::Display for Resolution {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Resolution::Single(r) => write!(f, "{}p", r),
            Resolution::Multiple(m) => {
                let mut iter = m.iter();
                for r in iter.by_ref().take(m.len() - 1) {
                    write!(f, "{}p/", r)?;
                }
                write!(f, "{}p", iter.next().unwrap())
            }
        }
    }
}

impl From<u32> for Resolution {
    fn from(s: u32) -> Self {
        Resolution::Single(s)
    }
}

impl From<[u32; 2]> for Resolution {
    fn from(a: [u32; 2]) -> Self {
        Resolution::Multiple(Vec::from(&a[..]))
    }
}
