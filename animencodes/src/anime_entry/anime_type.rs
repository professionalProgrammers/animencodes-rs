#[derive(Debug, Eq, PartialEq, Clone, Copy, serde::Serialize, serde::Deserialize)]
pub enum AnimeType {
    Sub,
    Dub,
}

impl AnimeType {
    pub fn as_str(self) -> &'static str {
        match self {
            AnimeType::Sub => "Sub",
            AnimeType::Dub => "Dub",
        }
    }
}

impl std::fmt::Display for AnimeType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.as_str())
    }
}
