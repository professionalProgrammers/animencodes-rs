use ouo::OuoUrl;
use shorte::ShorteUrl;
use std::str::FromStr;
use url::Url;

#[derive(Debug, Clone, Eq, PartialEq, serde::Serialize, serde::Deserialize)]
#[serde(from = "Url", into = "Url")]
pub enum Link {
    Spaste(Url),
    Shorte(ShorteUrl),
    Ouo(OuoUrl),
    Unknown(Url),
}

impl Link {
    pub fn is_spaste(&self) -> bool {
        matches!(self, Self::Spaste(_))
    }

    pub fn is_shorte(&self) -> bool {
        matches!(self, Self::Shorte(_))
    }

    pub fn is_ouo(&self) -> bool {
        matches!(self, Self::Ouo(_))
    }

    pub fn is_unknown(&self) -> bool {
        matches!(self, Self::Unknown(_))
    }

    pub fn as_url(&self) -> &Url {
        match self {
            Self::Spaste(u) => u,
            Self::Shorte(u) => u.as_url(),
            Self::Ouo(u) => u.as_url(),
            Self::Unknown(u) => u,
        }
    }

    pub fn into_url(self) -> Url {
        match self {
            Self::Spaste(u) => u,
            Self::Shorte(u) => u.into_url(),
            Self::Ouo(u) => u.into_url(),
            Self::Unknown(u) => u,
        }
    }
}

impl From<Url> for Link {
    fn from(u: Url) -> Self {
        if let Ok(url) = ShorteUrl::from_url(u.clone()) {
            return Link::Shorte(url);
        }

        if let Ok(url) = OuoUrl::from_url(u.clone()) {
            return Link::Ouo(url);
        }

        match u.host_str() {
            Some("www.spaste.com") => Link::Spaste(u),
            Some("spaste.com") => Link::Spaste(u),
            Some(_) => Link::Unknown(u),
            None => Link::Unknown(u),
        }
    }
}

impl Into<Url> for Link {
    fn into(self) -> Url {
        self.into_url()
    }
}

impl std::fmt::Display for Link {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.as_url())
    }
}

impl FromStr for Link {
    type Err = url::ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Url::parse(s)?.into())
    }
}
