use std::{
    convert::Infallible,
    str::FromStr,
};

#[derive(Clone, Debug, Eq, PartialEq, serde::Serialize, serde::Deserialize)]
pub enum Encoder {
    Cleo,
    Ranger,
    DB,
    Droid,
    Pixel,
    ManK,
    Phazer11,
    Bonkai77,
    HP,
    Forlorn,
    Multiple(Vec<Encoder>),
    Unknown(String),
}

impl Encoder {
    pub fn is_known(&self) -> bool {
        match self {
            Encoder::Unknown(_) => false,
            Encoder::Multiple(data) => data.iter().all(Self::is_known),
            _ => true,
        }
    }
}

impl FromStr for Encoder {
    type Err = Infallible;
    fn from_str(data: &str) -> Result<Self, Self::Err> {
        if data.contains('/') {
            return Ok(Encoder::Multiple(
                data.split('/')
                    .map(Encoder::from_str)
                    .collect::<Result<_, _>>()?,
            ));
        }

        if data.contains('-') {
            return Ok(Encoder::Multiple(
                data.split('-')
                    .map(Encoder::from_str)
                    .collect::<Result<_, _>>()?,
            ));
        }

        Ok(match data {
            "Cleo" => Encoder::Cleo,
            "Ranger" => Encoder::Ranger,
            "DB" => Encoder::DB,
            "Droid" => Encoder::Droid,
            "Pixel" => Encoder::Pixel,
            "Man.K" => Encoder::ManK,
            "bonkai77" => Encoder::Bonkai77,
            "phazer11" => Encoder::Phazer11,
            "HP" => Encoder::HP,
            "Forlorn" => Encoder::Forlorn,
            _ => Encoder::Unknown(String::from(data)),
        })
    }
}
