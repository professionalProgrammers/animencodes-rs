use crate::AnimeEntry;

#[derive(Debug, Clone)]
pub enum NewAnimeEntry {
    Anime(Box<AnimeEntry>),
    Unknown(String),
}

impl NewAnimeEntry {
    pub fn from_title(title: &str) -> Vec<Self> {
        match AnimeEntry::from_new_anime_name(title) {
            Ok(e) => e
                .into_iter()
                .map(|mut entry| {
                    entry.raw_name = Some(String::from(title));
                    NewAnimeEntry::Anime(Box::new(entry))
                })
                .collect(),
            Err(_) => vec![NewAnimeEntry::Unknown(String::from(title))],
        }
    }

    pub fn get_raw_name(&self) -> Option<&str> {
        match &self {
            NewAnimeEntry::Anime(a) => a.raw_name.as_deref(),
            NewAnimeEntry::Unknown(s) => Some(&s),
        }
    }
}
