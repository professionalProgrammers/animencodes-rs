pub mod anime_parser;
pub mod lexer;
pub mod new_anime_parser;
mod video_info;

pub use self::{
    anime_parser::{
        parse as parse_anime,
        AnimeNameParser,
        ParseError as AnimeNameParseError,
    },
    lexer::{
        AnimeNameLexer,
        LexerError,
        OwnedToken,
        Token,
    },
    new_anime_parser::{
        NewAnimeNameParser,
        ParseError as NewAnimeNameParseError,
    },
    video_info::VideoInfo,
};
use crate::{
    AnimeType,
    Encoding,
    Resolution,
};

pub(crate) struct NewAnimeNameData {
    pub name: String,
    pub encoding: Encoding,
    pub resolution: Option<Resolution>,
    pub anime_type: AnimeType,
}
