use crate::MainPageError;

pub type AnimeResult<T> = Result<T, AnimeError>;

#[derive(Debug)]
pub enum AnimeError {
    Reqwest(reqwest::Error),
    InvalidStatus(reqwest::StatusCode),
    Io(std::io::Error),
    InvalidMainPage(MainPageError),
}

impl From<reqwest::Error> for AnimeError {
    fn from(e: reqwest::Error) -> Self {
        Self::Reqwest(e)
    }
}

impl From<std::io::Error> for AnimeError {
    fn from(e: std::io::Error) -> Self {
        Self::Io(e)
    }
}

impl From<MainPageError> for AnimeError {
    fn from(e: MainPageError) -> Self {
        Self::InvalidMainPage(e)
    }
}
