mod anime_type;
mod encoder;
mod encoding;
mod link;
mod resolution;

pub use self::{
    anime_type::AnimeType,
    encoder::Encoder,
    encoding::Encoding,
    link::Link,
    resolution::Resolution,
};
use crate::parser::{
    parse_anime,
    AnimeNameParseError,
    NewAnimeNameData,
    NewAnimeNameParseError,
    NewAnimeNameParser,
};

#[derive(Clone, Debug, Eq, PartialEq, serde::Serialize, serde::Deserialize)]
pub struct AnimeEntry {
    pub raw_name: Option<String>,
    pub names: Option<Vec<String>>,
    pub link: Option<Link>,
    pub encoder: Option<Encoder>,
    pub encodings: Option<Vec<Encoding>>,
    pub resolution: Option<Resolution>,
    pub anime_type: Option<AnimeType>,
}

impl AnimeEntry {
    pub fn from_new_anime_name(name: &str) -> Result<Vec<Self>, NewAnimeNameParseError> {
        Ok(NewAnimeNameParser::new().parse(name)?)
    }

    pub fn from_name(name: &str) -> Result<Self, AnimeNameParseError> {
        parse_anime(name)
    }
}

impl Default for AnimeEntry {
    fn default() -> Self {
        AnimeEntry {
            raw_name: None,
            names: None,
            link: None,
            encoder: None,
            encodings: None,
            resolution: None,
            anime_type: None,
        }
    }
}

impl From<NewAnimeNameData> for AnimeEntry {
    fn from(data: NewAnimeNameData) -> Self {
        AnimeEntry {
            raw_name: None,
            names: Some(vec![data.name]),
            link: None,
            encoder: None,
            encodings: Some(vec![data.encoding]),
            resolution: data.resolution,
            anime_type: Some(data.anime_type),
        }
    }
}
