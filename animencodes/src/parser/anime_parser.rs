pub use self::anime_parser::AnimeNameParser;
use crate::{
    parser::{
        AnimeNameLexer,
        AnimeNameParseError,
        LexerError,
        OwnedToken,
        Token,
    },
    AnimeEntry,
    AnimeType,
    Encoder,
    Encoding,
    Resolution,
};
use lalrpop_util::lalrpop_mod;

lalrpop_mod!(
    #[allow(clippy::all)]
    anime_parser
);

type InnerParseError<'a> = lalrpop_util::ParseError<usize, Token<'a>, LexerError>;
type InnerOwnedParseError = lalrpop_util::ParseError<usize, OwnedToken, LexerError>;

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct ParseError<'a>(pub InnerParseError<'a>);

impl<'a> From<InnerParseError<'a>> for ParseError<'a> {
    fn from(e: InnerParseError<'a>) -> Self {
        Self(e)
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct OwnedParseError(pub InnerOwnedParseError);

impl<'a> From<ParseError<'a>> for OwnedParseError {
    fn from(e: ParseError<'a>) -> Self {
        Self(e.0.map_token(Into::into))
    }
}

/// Try to parse an anime name into an anime entry.
/// This is the preferred way of parsing a name, as it has internal overrides for malformed names present on the website.
pub fn parse(name: &str) -> Result<AnimeEntry, AnimeNameParseError> {
    match name {
        "[Cleo] C: The Money of Soul and Possibility Control | [C] CONTROL - The Money and Soul of Possibility [Dual Audio 10bit BD1080p][HEVC-x265]" => {
            Ok(AnimeEntry {
                raw_name: Some(name.to_string()),
                names: Some(vec![String::from("C: The Money of Soul and Possibility Control"), String::from("[C] CONTROL - The Money and Soul of Possibility")]),
                encoder: Some(Encoder::Cleo),
                encodings: Some(vec![Encoding::H265]),
                resolution: Some(Resolution::Single(1080)),
                anime_type: Some(AnimeType::Dub),
                link: None,
            })
        },
        "[Cleo] Senkou no Night Raid | Night Raid 1931 [Dual Audio 10bit BD1080p[HEVC-x265]" => {
             Ok(AnimeEntry {
                raw_name: Some(name.to_string()),
                names: Some(vec![String::from("Senkou no Night Raid"), String::from("Night Raid 1931")]),
                encoder: Some(Encoder::Cleo),
                encodings: Some(vec![Encoding::H265]),
                resolution: Some(Resolution::Single(1080)),
                anime_type: Some(AnimeType::Dub),
                link: None,
            })
        },
        "[Cleo] Sennen Joyuu | Millennium Actress [Dual Audio 10bit BD1080p[HEVC-x265]" => {
            Ok(AnimeEntry {
                raw_name: Some(name.to_string()),
                names: Some(vec![String::from("Sennen Joyuu"), String::from("Millennium Actress")]),
                encoder: Some(Encoder::Cleo),
                encodings: Some(vec![Encoding::H265]),
                resolution: Some(Resolution::Single(1080)),
                anime_type: Some(AnimeType::Dub),
                link: None,
            })
        },
        "[Cleo] Senran Kagura: Ninja Flash [Dual Audio 10bit BD1080p[HEVC-x265]" => {
            Ok(AnimeEntry {
                raw_name: Some(name.to_string()),
                names: Some(vec![String::from("Senran Kagura: Ninja Flash")]),
                encoder: Some(Encoder::Cleo),
                encodings: Some(vec![Encoding::H265]),
                resolution: Some(Resolution::Single(1080)),
                anime_type: Some(AnimeType::Dub),
                link: None,
            })
        },
        "[Cleo] Serial Experiments Lain [Dual Audio 10bit BD1080p[HEVC-x265]" => {
            Ok(AnimeEntry {
                raw_name: Some(name.to_string()),
                names: Some(vec![String::from("Serial Experiments Lain")]),
                encoder: Some(Encoder::Cleo),
                encodings: Some(vec![Encoding::H265]),
                resolution: Some(Resolution::Single(1080)),
                anime_type: Some(AnimeType::Dub),
                link: None,
            })
        }
        "[Cleo] Senran Kagura Shinovi Master: Tokyo Youma-hen | Senran Kagura: Shinovi Master [Dual Audio 10bit BD1080p[HEVC-x265]" => {
            Ok(AnimeEntry {
                raw_name: Some(name.to_string()),
                names: Some(vec![String::from("Senran Kagura Shinovi Master: Tokyo Youma-hen"), String::from("Senran Kagura: Shinovi Master")]),
                encoder: Some(Encoder::Cleo),
                encodings: Some(vec![Encoding::H265]),
                resolution: Some(Resolution::Single(1080)),
                anime_type: Some(AnimeType::Dub),
                link: None,
            })
        },
        _=> {
            let lexer = AnimeNameLexer::new(name);
            Ok(AnimeNameParser::new().parse(name, lexer).map(|mut entry| {
                entry.raw_name = Some(name.into());
                entry
            })?)
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    // Pretty useless since creating a main page invokes the parser implicitly, but maybe useful for creating/experimenting with a new parser
    #[ignore]
    #[test]
    fn lexer_main_page() {
        use crate::{
            Document,
            MainPage,
        };

        let s = include_str!("../../test_data/animencodes.html");
        let doc = Document::from(s);
        let page = MainPage::from_doc(&doc).unwrap();

        for entry in page.dubbed_entries() {
            let name = entry.raw_name.as_deref().unwrap();
            dbg!(name);
            let _res = parse(name).unwrap();
        }

        for entry in page.subbed_entries() {
            let name = entry.raw_name.as_deref().unwrap();
            dbg!(name);
            let _res = parse(name).unwrap();
        }
    }

    #[test]
    fn parse_simple() {
        let name = "[Cleo] Naruto [Dual Audio 10bit DVD480p][HEVC-x265]";
        let res = parse(name).unwrap();
        dbg!(res);
    }

    #[test]
    fn parse_medium() {
        let name = "[Cleo] Accel World: Infinite∞Burst | Accel World: Infinite Burst [Dual Audio 10bit BD1080p][HEVC-x265]";
        let res = parse(name).unwrap();
        dbg!(res);
    }

    #[test]
    fn parse_irregular_n() {
        let name = "[Ranger] Baccano! [N][Dual Audio 10bit BD720p][HEVC-x265]";
        let res = parse(name).unwrap();
        dbg!(res);
    }

    #[test]
    fn lexer_parse_triple_name() {
        let name = "[Cleo/DB] Yuragi-sou no Yuuna-san | Yuuna of Yuragi Manor | Yuuna and the Haunted Hot Springs [10bit BD1080p][HEVC-x265]";
        let res = parse(name).unwrap();
        dbg!(res);
    }
}
