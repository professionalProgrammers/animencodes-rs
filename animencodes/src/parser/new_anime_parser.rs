pub use self::new_anime_parser::{
    NewAnimeNameParser,
    Token,
};
use lalrpop_util::lalrpop_mod;

lalrpop_mod!(
    #[allow(clippy::all)]
    new_anime_parser
);

type InnerParseError<'a> = lalrpop_util::ParseError<usize, Token<'a>, &'static str>;
type OwnedInnerParseError = lalrpop_util::ParseError<usize, OwnedToken, &'static str>;

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct ParseError<'a>(pub InnerParseError<'a>);

impl<'a> From<InnerParseError<'a>> for ParseError<'a> {
    fn from(e: InnerParseError<'a>) -> Self {
        Self(e)
    }
}

pub struct OwnedParseError(pub OwnedInnerParseError);

impl<'a> From<ParseError<'a>> for OwnedParseError {
    fn from(e: ParseError<'a>) -> Self {
        Self(e.0.map_token(Into::into))
    }
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct OwnedToken(pub usize, pub String);

impl<'a> From<Token<'a>> for OwnedToken {
    fn from(t: Token) -> Self {
        Self(t.0, String::from(t.1))
    }
}
