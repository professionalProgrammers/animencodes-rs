use crate::{
    AnimeType,
    Resolution,
};

pub struct VideoInfo {
    pub(crate) anime_type: AnimeType,
    pub(crate) is_10_bit: bool,
    pub(crate) resolution: Option<Resolution>,
}

impl VideoInfo {
    pub(crate) fn from_data_block_str(s: &str) -> Option<Self> {
        let mut anime_type = AnimeType::Sub;
        let mut is_10_bit = false;
        let mut resolution = None;

        let mut iter = s.split(' ').peekable();

        while let Some(el) = iter.next() {
            match el {
                "Dual" => {
                    if iter.peek() == Some(&"Audio") {
                        iter.next();
                        anime_type = AnimeType::Dub;
                    } else {
                        return None;
                    }
                }
                "10bit" => {
                    is_10_bit = true;
                }
                "0bit" => {
                    is_10_bit = true; // Accept Error
                }
                "BD1080p" | "1080p" => {
                    resolution = Some(Resolution::from(1080));
                }
                "BD960p" => {
                    resolution = Some(Resolution::from(960));
                }
                "BD720p" | "720p" => {
                    resolution = Some(Resolution::from(720));
                }
                "DVD576p" => {
                    resolution = Some(Resolution::from(576));
                }
                "DVD480p" | "480p" => {
                    resolution = Some(Resolution::from(480));
                }
                "BD544p" => {
                    resolution = Some(Resolution::from(544));
                }
                "BD720/1080p" => {
                    resolution = Some(Resolution::from([1080, 720]));
                }
                "DVD480p/BD1080p" => {
                    resolution = Some(Resolution::from([480, 1080]));
                }
                _ => return None,
            }
        }

        Some(VideoInfo {
            anime_type,
            is_10_bit,
            resolution,
        })
    }
}
