use std::{
    iter::Peekable,
    str::CharIndices,
};

pub type Spanned<Token, Location, Error> = Result<(Location, Token, Location), Error>;

#[derive(Debug, PartialEq, Eq, Copy, Clone, Hash)]
pub enum Token<'a> {
    LBracket,
    RBracket,
    Line,

    Ident(&'a str),
    Encoding(&'a str),

    N,
    Dubbed,
    HardSubbed,
}

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
pub enum OwnedToken {
    LBracket,
    RBracket,
    Line,

    Ident(String),
    Encoding(String),

    N,
    Dubbed,
    HardSubbed,
}

impl From<Token<'_>> for OwnedToken {
    fn from(token: Token<'_>) -> Self {
        match token {
            Token::LBracket => OwnedToken::LBracket,
            Token::RBracket => OwnedToken::RBracket,
            Token::Line => OwnedToken::Line,
            Token::Ident(s) => OwnedToken::Ident(s.into()),
            Token::Encoding(s) => OwnedToken::Encoding(s.into()),
            Token::N => OwnedToken::N,
            Token::Dubbed => OwnedToken::Dubbed,
            Token::HardSubbed => OwnedToken::HardSubbed,
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub enum LexerError {
    UnexpectedEof,
}

#[derive(Debug, Clone)]
pub struct AnimeNameLexer<'a> {
    chars: Peekable<CharIndices<'a>>,
    input: &'a str,
}

impl<'a> AnimeNameLexer<'a> {
    pub fn new(input: &'a str) -> Self {
        Self {
            chars: input.char_indices().peekable(),
            input,
        }
    }

    fn get_next_token(&mut self) -> Option<<Self as Iterator>::Item> {
        while self.chars.peek()?.1.is_whitespace() {
            self.chars.next()?;
        }

        match self.chars.next()? {
            (i, '[') => Some(Ok((i, Token::LBracket, i + 1))),
            (i, ']') => Some(Ok((i, Token::RBracket, i + 1))),
            (i, '|') => Some(Ok((i, Token::Line, i + 1))),
            (start, _) => self.lex_ident(start),
        }
    }

    #[inline]
    #[allow(clippy::unnecessary_wraps)]
    fn lex_ident(&mut self, start: usize) -> Option<<Self as Iterator>::Item> {
        let mut end = start;
        while let Some(next) = self.chars.peek() {
            if next.1 == '[' || next.1 == '|' || next.1 == ']' {
                break;
            }

            end = self.chars.next().unwrap().0;
        }

        let ident = self.input[start..end + 1].trim();

        let token = match ident {
            "HEVC-x265" | "Hevc" | "H.264 and H.265" => Token::Encoding(ident),
            "N" => Token::N,
            "Dubbed" => Token::Dubbed,
            "-HARDSUBBED-" => Token::HardSubbed,
            _ => Token::Ident(ident),
        };

        Some(Ok((start, token, end + 1)))
    }
}

impl<'a> Iterator for AnimeNameLexer<'a> {
    type Item = Spanned<Token<'a>, usize, LexerError>;

    fn next(&mut self) -> Option<Self::Item> {
        self.get_next_token()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn lexer_simple() {
        let name = "[Cleo] Naruto [Dual Audio 10bit DVD480p][HEVC-x265]";
        let mut lexer = AnimeNameLexer::new(name);
        assert_eq!(lexer.next().unwrap().unwrap(), (0, Token::LBracket, 1));
        assert_eq!(lexer.next().unwrap().unwrap(), (1, Token::Ident("Cleo"), 5));
        assert_eq!(lexer.next().unwrap().unwrap(), (5, Token::RBracket, 6));
        assert_eq!(
            lexer.next().unwrap().unwrap(),
            (7, Token::Ident("Naruto"), 14)
        );
        assert_eq!(lexer.next().unwrap().unwrap(), (14, Token::LBracket, 15));
        assert_eq!(
            lexer.next().unwrap().unwrap(),
            (15, Token::Ident("Dual Audio 10bit DVD480p"), 39)
        );
        assert_eq!(lexer.next().unwrap().unwrap(), (39, Token::RBracket, 40));
        assert_eq!(lexer.next().unwrap().unwrap(), (40, Token::LBracket, 41));
        assert_eq!(
            lexer.next().unwrap().unwrap(),
            (41, Token::Encoding("HEVC-x265"), 50)
        );
        assert_eq!(lexer.next().unwrap().unwrap(), (50, Token::RBracket, 51));
        assert!(lexer.next().is_none());
    }

    #[test]
    fn lexer_complex() {
        let name = "[Cleo] C: The Money of Soul and Possibility Control | [C] CONTROL - The Money and Soul of Possibility [Dual Audio 10bit BD1080p][HEVC-x265]";
        let mut lexer = AnimeNameLexer::new(name);
        assert_eq!(lexer.next().unwrap().unwrap(), (0, Token::LBracket, 1));
        assert_eq!(lexer.next().unwrap().unwrap(), (1, Token::Ident("Cleo"), 5));
        assert_eq!(lexer.next().unwrap().unwrap(), (5, Token::RBracket, 6));
        assert_eq!(
            lexer.next().unwrap().unwrap(),
            (
                7,
                Token::Ident("C: The Money of Soul and Possibility Control"),
                52
            )
        );
        assert_eq!(lexer.next().unwrap().unwrap(), (52, Token::Line, 53));
        // This is part of the name. We can fix this with a parser override, but this should still at least lex.
        // TODO: Maybe add some form of context-aware lexing too?
        assert_eq!(lexer.next().unwrap().unwrap(), (54, Token::LBracket, 55));
        assert_eq!(lexer.next().unwrap().unwrap(), (55, Token::Ident("C"), 56));
        assert_eq!(lexer.next().unwrap().unwrap(), (56, Token::RBracket, 57));
        assert_eq!(
            lexer.next().unwrap().unwrap(),
            (
                58,
                Token::Ident("CONTROL - The Money and Soul of Possibility"),
                102
            )
        );
        assert_eq!(lexer.next().unwrap().unwrap(), (102, Token::LBracket, 103));
        assert_eq!(
            lexer.next().unwrap().unwrap(),
            (103, Token::Ident("Dual Audio 10bit BD1080p"), 127)
        );
        assert_eq!(lexer.next().unwrap().unwrap(), (127, Token::RBracket, 128));
        assert_eq!(lexer.next().unwrap().unwrap(), (128, Token::LBracket, 129));
        assert_eq!(
            lexer.next().unwrap().unwrap(),
            (129, Token::Encoding("HEVC-x265"), 138)
        );
        assert_eq!(lexer.next().unwrap().unwrap(), (138, Token::RBracket, 139));
        assert!(lexer.next().is_none());
    }

    #[test]
    fn lexer_irregular_n() {
        let name = "[Ranger] Baccano! [N][Dual Audio 10bit BD720p][HEVC-x265]";
        let mut lexer = AnimeNameLexer::new(name);
        assert_eq!(lexer.next().unwrap().unwrap(), (0, Token::LBracket, 1));
        assert_eq!(
            lexer.next().unwrap().unwrap(),
            (1, Token::Ident("Ranger"), 7)
        );
        assert_eq!(lexer.next().unwrap().unwrap(), (7, Token::RBracket, 8));
        assert_eq!(
            lexer.next().unwrap().unwrap(),
            (9, Token::Ident("Baccano!"), 18)
        );
        assert_eq!(lexer.next().unwrap().unwrap(), (18, Token::LBracket, 19));
        assert_eq!(lexer.next().unwrap().unwrap(), (19, Token::N, 20));
        assert_eq!(lexer.next().unwrap().unwrap(), (20, Token::RBracket, 21));
        assert_eq!(lexer.next().unwrap().unwrap(), (21, Token::LBracket, 22));
        assert_eq!(
            lexer.next().unwrap().unwrap(),
            (22, Token::Ident("Dual Audio 10bit BD720p"), 45)
        );
        assert_eq!(lexer.next().unwrap().unwrap(), (45, Token::RBracket, 46));
        assert_eq!(lexer.next().unwrap().unwrap(), (46, Token::LBracket, 47));
        assert_eq!(
            lexer.next().unwrap().unwrap(),
            (47, Token::Encoding("HEVC-x265"), 56)
        );
        assert_eq!(lexer.next().unwrap().unwrap(), (56, Token::RBracket, 57));
        assert!(lexer.next().is_none());
    }

    #[test]
    fn lexer_new_simple() {
        let name = "BNA [Hevc][Dual]";
        let mut lexer = AnimeNameLexer::new(name);
        assert_eq!(lexer.next().unwrap().unwrap(), (0, Token::Ident("BNA"), 4));
        assert_eq!(lexer.next().unwrap().unwrap(), (4, Token::LBracket, 5));
        assert_eq!(
            lexer.next().unwrap().unwrap(),
            (5, Token::Encoding("Hevc"), 9)
        );
        assert_eq!(lexer.next().unwrap().unwrap(), (9, Token::RBracket, 10));
        assert_eq!(lexer.next().unwrap().unwrap(), (10, Token::LBracket, 11));
        assert_eq!(
            lexer.next().unwrap().unwrap(),
            (11, Token::Ident("Dual"), 15)
        );
        assert_eq!(lexer.next().unwrap().unwrap(), (15, Token::RBracket, 16));
        assert!(lexer.next().is_none());
    }

    #[test]
    fn lexer_new_resolution() {
        let name = "Kami no Tou [1080][Hevc][Dual]";
        let mut lexer = AnimeNameLexer::new(name);
        assert_eq!(
            lexer.next().unwrap().unwrap(),
            (0, Token::Ident("Kami no Tou"), 12)
        );
        assert_eq!(lexer.next().unwrap().unwrap(), (12, Token::LBracket, 13));
        assert_eq!(
            lexer.next().unwrap().unwrap(),
            (13, Token::Ident("1080"), 17)
        );
        assert_eq!(lexer.next().unwrap().unwrap(), (17, Token::RBracket, 18));
        assert_eq!(lexer.next().unwrap().unwrap(), (18, Token::LBracket, 19));
        assert_eq!(
            lexer.next().unwrap().unwrap(),
            (19, Token::Encoding("Hevc"), 23)
        );
        assert_eq!(lexer.next().unwrap().unwrap(), (23, Token::RBracket, 24));
        assert_eq!(lexer.next().unwrap().unwrap(), (24, Token::LBracket, 25));
        assert_eq!(
            lexer.next().unwrap().unwrap(),
            (25, Token::Ident("Dual"), 29)
        );
        assert_eq!(lexer.next().unwrap().unwrap(), (29, Token::RBracket, 30));
        assert!(lexer.next().is_none());
    }

    #[test]
    fn lexer_new_multi() {
        let name = "Dorei-ku The Animation [1080][Hevc][Dual] | Eromanga-sensei [1080][Hevc][Sub]";
        let mut lexer = AnimeNameLexer::new(name);
        assert_eq!(
            lexer.next().unwrap().unwrap(),
            (0, Token::Ident("Dorei-ku The Animation"), 23)
        );
        assert_eq!(lexer.next().unwrap().unwrap(), (23, Token::LBracket, 24));
        assert_eq!(
            lexer.next().unwrap().unwrap(),
            (24, Token::Ident("1080"), 28)
        );
        assert_eq!(lexer.next().unwrap().unwrap(), (28, Token::RBracket, 29));
        assert_eq!(lexer.next().unwrap().unwrap(), (29, Token::LBracket, 30));
        assert_eq!(
            lexer.next().unwrap().unwrap(),
            (30, Token::Encoding("Hevc"), 34)
        );
        assert_eq!(lexer.next().unwrap().unwrap(), (34, Token::RBracket, 35));
        assert_eq!(lexer.next().unwrap().unwrap(), (35, Token::LBracket, 36));
        assert_eq!(
            lexer.next().unwrap().unwrap(),
            (36, Token::Ident("Dual"), 40)
        );
        assert_eq!(lexer.next().unwrap().unwrap(), (40, Token::RBracket, 41));
        assert_eq!(lexer.next().unwrap().unwrap(), (42, Token::Line, 43));
        assert_eq!(
            lexer.next().unwrap().unwrap(),
            (44, Token::Ident("Eromanga-sensei"), 60)
        );
        assert_eq!(lexer.next().unwrap().unwrap(), (60, Token::LBracket, 61));
        assert_eq!(
            lexer.next().unwrap().unwrap(),
            (61, Token::Ident("1080"), 65)
        );
        assert_eq!(lexer.next().unwrap().unwrap(), (65, Token::RBracket, 66));
        assert_eq!(lexer.next().unwrap().unwrap(), (66, Token::LBracket, 67));
        assert_eq!(
            lexer.next().unwrap().unwrap(),
            (67, Token::Encoding("Hevc"), 71)
        );
        assert_eq!(lexer.next().unwrap().unwrap(), (71, Token::RBracket, 72));
        assert_eq!(lexer.next().unwrap().unwrap(), (72, Token::LBracket, 73));
        assert_eq!(
            lexer.next().unwrap().unwrap(),
            (73, Token::Ident("Sub"), 76)
        );
        assert_eq!(lexer.next().unwrap().unwrap(), (76, Token::RBracket, 77));
        assert!(lexer.next().is_none());
    }
}
