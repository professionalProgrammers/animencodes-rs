pub mod list;
pub mod resolve;
pub mod search;

use crate::AnimeEntry;
use clap::{
    App,
    AppSettings,
    ArgMatches,
};
use prettytable::{
    Cell,
    Row,
    Table,
};
use std::borrow::Borrow;

pub fn cli() -> App<'static, 'static> {
    App::new("animencodes")
        .setting(AppSettings::SubcommandRequired)
        .subcommand(self::search::cli())
        .subcommand(self::list::cli())
        .subcommand(self::resolve::cli())
        .subcommand(shorte::cli::cli())
}

pub fn exec(matches: &ArgMatches) {
    match matches.subcommand() {
        ("search", Some(matches)) => self::search::exec(&matches),
        ("list", Some(matches)) => self::list::exec(&matches),
        ("resolve", Some(matches)) => self::resolve::exec(&matches),
        ("shorte", Some(matches)) => shorte::cli::exec(&matches),
        (command, _) => println!("Unknown Command '{}'", command),
    }
}

pub fn tabulate_anime_entry<T: Iterator<Item = I>, I: Borrow<AnimeEntry>>(iter: T) -> Table {
    let table_header = [Row::new(vec![
        (&"Anime Name").into(),
        (&"Anime Type").into(),
        (&"Resolution").into(),
        (&"Link").into(),
    ])];

    table_header
        .iter()
        .cloned()
        .chain(iter.map(|el| tabulate_entry(el.borrow())))
        .collect()
}

pub fn tabulate_entry(entry: &AnimeEntry) -> Row {
    let unknown_cell = Cell::new("Unknown");

    let name = entry
        .names
        .as_ref()
        .map(|names| names.join("\n"))
        .or_else(|| entry.raw_name.clone())
        .unwrap_or_else(|| "Unknown".into());

    let name_cell = Cell::new(&name.replace(" | ", "\n"));

    let kind = entry
        .anime_type
        .as_ref()
        .map(|el| (&el).into())
        .unwrap_or_else(|| unknown_cell.clone());

    let resolution = entry
        .resolution
        .as_ref()
        .map(Into::into)
        .unwrap_or_else(|| unknown_cell.clone());

    let link = entry
        .link
        .as_ref()
        .map(Into::into)
        .unwrap_or_else(|| unknown_cell.clone());

    Row::new(vec![name_cell, kind, resolution, link])
}
