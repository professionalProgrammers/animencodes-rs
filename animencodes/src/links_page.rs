use crate::Link;
use select::{
    document::Document,
    predicate::{
        Attr,
        Class,
        Name,
        Text,
    },
};
use std::str::FromStr;

/// An element of the LinksPage
#[derive(Debug, PartialEq)]
pub enum LinksPageElement {
    /// A Text Chunk
    Text(String),

    /// An index into a links page.
    Link(usize),

    /// A Newline char
    Newline,
}

/// An error that may occur while creating a links page
#[derive(Debug)]
pub enum LinksPageError {
    MissingMainDiv,
    MissingInfoDiv,

    MissingTitle,

    MissingAuthor,

    MissingUrl,
    InvalidUrl(url::ParseError),
}

/// A page of Animencodes Links, aquired through spaste.
#[derive(Debug)]
pub struct LinksPage {
    page: Vec<LinksPageElement>,
    links: Vec<Link>,

    /// Page Title
    pub title: String,

    /// Page Author
    pub author: String,
}

impl LinksPage {
    /// Try to create a links page from a document
    pub fn from_doc(doc: &Document) -> Result<Self, LinksPageError> {
        let main_div = doc
            .find(Attr("id", "template-contactform-message"))
            .last()
            .ok_or(LinksPageError::MissingMainDiv)?;

        let info_div = doc
            .find(Class("well"))
            .last()
            .ok_or(LinksPageError::MissingInfoDiv)?;

        let title = info_div
            .find(Name("strong"))
            .filter_map(|el| el.find(Text).last()?.as_text().map(|t| t.trim()))
            .filter(|t| !t.is_empty())
            .last()
            .ok_or(LinksPageError::MissingTitle)?
            .to_string();

        let author = info_div
            .find(Name("p"))
            .last()
            .ok_or(LinksPageError::MissingAuthor)?
            .find(Text)
            .filter_map(|el| el.as_text())
            .filter(|&el| el != "By")
            .find(|t| !t.trim().is_empty())
            .map(|t| t.trim().trim_matches(':').trim_start().to_string())
            .ok_or(LinksPageError::MissingAuthor)?;

        let node_stream = main_div.children().skip_while(|node| {
            !node
                .next()
                .and_then(|n| Link::from_str(n.attr("href")?).ok())
                .map_or(false, |l| l.is_shorte() || l.is_ouo())
        });

        let mut links = Vec::new();
        let mut page = Vec::new();

        for node in node_stream {
            if let Some(text) = node.as_text() {
                if !text.trim().is_empty() {
                    page.push(LinksPageElement::Text(String::from(text.trim_start())));
                }
            } else if node.is(Name("a")) {
                let href = node.attr("href").ok_or(LinksPageError::MissingUrl)?;
                let link = Link::from_str(href).map_err(LinksPageError::InvalidUrl)?;
                page.push(LinksPageElement::Link(links.len()));
                links.push(link);
            } else if node.is(Name("br")) {
                page.push(LinksPageElement::Newline);
            }
        }

        Ok(Self {
            title,
            page,
            links,
            author,
        })
    }

    /// Get a reference to the internal links storage
    pub fn links(&self) -> &[Link] {
        &self.links
    }

    /// Get a mutable reference to the internal links storage
    pub fn links_mut(&mut self) -> &mut [Link] {
        &mut self.links
    }

    /// Get an optional reference to a link in the internal link storage
    pub fn get_link(&self, i: usize) -> Option<&Link> {
        self.links.get(i)
    }

    /// Get a mutable optional reference to a link in the internal link storage
    pub fn get_link_mut(&mut self, i: usize) -> Option<&mut Link> {
        self.links.get_mut(i)
    }

    /// Render this object as text
    pub fn render(&self) -> String {
        let mut ret = String::new();
        ret += &self.title;
        ret += "\nBy: ";
        ret += &self.author;
        ret += "\n\n";
        for el in self.page.iter() {
            match el {
                LinksPageElement::Text(txt) => ret += txt,
                LinksPageElement::Link(i) => ret += self.links[*i].as_url().as_str(),
                LinksPageElement::Newline => ret.push('\n'),
            }
        }

        ret
    }
}

impl FromStr for LinksPage {
    type Err = LinksPageError;

    /// Tries to create a linkspage from a document string
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let doc = Document::from(s);
        Self::from_doc(&doc)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    pub fn parse_links_page() {
        const DATA: &[&str] = &[
            include_str!("../test_data/links_page_4.html"),
            include_str!("../test_data/links_page_3.html"),
            include_str!("../test_data/links_page_2.html"),
            include_str!("../test_data/links_page_1.html"),
        ];

        for html in DATA {
            let page = LinksPage::from_doc(&Document::from(*html)).unwrap();
            println!("\n{}\n", page.render());
        }
    }
}
