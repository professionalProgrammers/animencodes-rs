use crate::{
    AnimeError,
    AnimeResult,
    MainPage,
};
use bytes::buf::BufExt;
use select::document::Document;

#[derive(Default)]
pub struct Client {
    client: reqwest::Client,
}

impl Client {
    pub fn new() -> Self {
        Default::default()
    }

    pub async fn get_main_page(&self) -> AnimeResult<MainPage> {
        let res = self
            .client
            .get("https://www.animencodes.com/")
            .send()
            .await?;

        let status = res.status();
        if !status.is_success() {
            return Err(AnimeError::InvalidStatus(status));
        }

        let body = res.bytes().await?.reader();
        let doc = Document::from_read(body)?;
        let page = MainPage::from_doc(&doc)?;

        Ok(page)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    fn assert_send<T: Send>(_t: T) {}

    #[test]
    fn static_assertions() {
        let client = Client::new();
        let get_main_page = client.get_main_page();

        assert_send(get_main_page);
        assert_send(client);
    }
}
