use crate::cli::tabulate_anime_entry;
use clap::{
    App,
    Arg,
    ArgMatches,
    SubCommand,
};
pub fn cli() -> App<'static, 'static> {
    SubCommand::with_name("search").arg(Arg::with_name("query").required(true))
}

pub fn exec(matches: &ArgMatches) {
    let query = matches.value_of("query").unwrap();
    let query_lowercase = query.to_lowercase();
    let client = crate::Client::new();

    let mut rt = match tokio::runtime::Runtime::new() {
        Ok(rt) => rt,
        Err(e) => {
            println!("Failed to init tokio runtime, got error: {:#?}", e);
            return;
        }
    };

    println!("Searching for '{}'...", query);
    println!();

    let main_page = match rt.block_on(client.get_main_page()) {
        Ok(p) => p,
        Err(e) => {
            println!("Failed to get main page, got error: {:#?}", e);
            return;
        }
    };

    let table = tabulate_anime_entry(
        main_page
            .dubbed_entries()
            .iter()
            .chain(main_page.subbed_entries().iter())
            .filter(|entry| {
                entry
                    .raw_name
                    .as_deref()
                    .map_or(false, |name| name.to_lowercase().contains(&query_lowercase))
            }),
    );

    table.printstd();
    println!();
}
