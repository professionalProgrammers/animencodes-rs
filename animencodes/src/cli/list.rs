use crate::{
    cli::tabulate_anime_entry,
    AnimeEntry,
    AnimeType,
    NewAnimeEntry,
};
use clap::{
    App,
    Arg,
    ArgGroup,
    ArgMatches,
    SubCommand,
};

#[derive(Debug, Copy, Clone)]
enum ListOption {
    NewSub,
    NewDub,
    NewSubDub,
    Sub,
    Dub,
    SubDub,
}

impl ListOption {
    fn from_matches(matches: &ArgMatches) -> Self {
        match (
            matches.is_present("new"),
            matches.is_present("sub"),
            matches.is_present("dub"),
        ) {
            (true, false, true) => Self::NewDub,
            (true, true, false) => Self::NewSub,
            (true, _, _) => Self::NewSubDub,
            (false, true, false) => Self::Sub,
            (false, false, true) => Self::Dub,
            (false, true, true) => Self::SubDub,
            (false, false, false) => Self::SubDub,
        }
    }

    fn as_str(self) -> &'static str {
        match self {
            ListOption::NewSub => "new subbed",
            ListOption::NewDub => "new dubbed",
            ListOption::NewSubDub => "new subbed and dubbed",
            ListOption::Sub => "subbed",
            ListOption::Dub => "dubbed",
            ListOption::SubDub => "subbed and dubbed",
        }
    }
}

impl std::fmt::Display for ListOption {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.as_str())
    }
}

pub fn cli() -> App<'static, 'static> {
    SubCommand::with_name("list")
        .arg(Arg::with_name("sub").long("sub").short("s"))
        .arg(Arg::with_name("dub").long("dub").short("d"))
        .arg(Arg::with_name("new").long("new").short("n"))
        .group(
            ArgGroup::with_name("type")
                .args(&["sub", "dub", "new"])
                .multiple(true),
        )
}

pub fn exec(matches: &ArgMatches) {
    let option = ListOption::from_matches(matches);
    let client = crate::Client::new();

    let mut rt = match tokio::runtime::Runtime::new() {
        Ok(rt) => rt,
        Err(e) => {
            println!("Failed to init tokio runtime, got error: {:#?}", e);
            return;
        }
    };

    println!("Getting {} entries...", option);
    println!();

    let main_page = match rt.block_on(client.get_main_page()) {
        Ok(p) => p,
        Err(e) => {
            println!("Failed to get main page, got error: {:#?}", e);
            return;
        }
    };

    let table = match option {
        ListOption::NewSub => tabulate_anime_entry(
            main_page
                .new_anime()
                .iter()
                .cloned()
                .map(to_anime_entry)
                .filter(|e| e.anime_type.map_or(true, |t| t == AnimeType::Sub)),
        ),
        ListOption::NewDub => tabulate_anime_entry(
            main_page
                .new_anime()
                .iter()
                .cloned()
                .map(to_anime_entry)
                .filter(|e| e.anime_type.map_or(true, |t| t == AnimeType::Dub)),
        ),
        ListOption::NewSubDub => {
            tabulate_anime_entry(main_page.new_anime().iter().cloned().map(to_anime_entry))
        }
        ListOption::Sub => tabulate_anime_entry(main_page.subbed_entries().iter()),
        ListOption::Dub => tabulate_anime_entry(main_page.dubbed_entries().iter()),
        ListOption::SubDub => tabulate_anime_entry(
            main_page
                .dubbed_entries()
                .iter()
                .chain(main_page.subbed_entries().iter()),
        ),
    };

    table.printstd();
}

fn to_anime_entry(entry: NewAnimeEntry) -> AnimeEntry {
    match entry {
        NewAnimeEntry::Anime(anime) => *anime,
        NewAnimeEntry::Unknown(s) => AnimeEntry {
            raw_name: Some(s),
            ..Default::default()
        },
    }
}
