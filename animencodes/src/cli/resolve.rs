use crate::{
    resolve_many_par,
    Link,
    LinksPage,
};
use clap::{
    App,
    Arg,
    ArgMatches,
    SubCommand,
};
use hcaptcha::RequestType;
use select::document::Document;
use spaste::Captcha;
use std::{
    io::Write,
    path::Path,
    time::Instant,
};
use url::Url;

fn read_input() -> String {
    use std::io::{
        stdin,
        stdout,
    };

    let mut s = String::new();

    let _ = stdout().flush();
    let _ = stdin().read_line(&mut s).is_ok();

    if let Some('\n') = s.chars().next_back() {
        s.pop();
    }
    if let Some('\r') = s.chars().next_back() {
        s.pop();
    }

    s
}

#[derive(Debug)]
pub struct GenericError(String);

impl<'a> From<&'a str> for GenericError {
    fn from(e: &'a str) -> Self {
        GenericError(e.into())
    }
}

impl From<String> for GenericError {
    fn from(e: String) -> Self {
        GenericError(e)
    }
}

impl std::fmt::Display for GenericError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl std::error::Error for GenericError {}

pub fn cli() -> App<'static, 'static> {
    SubCommand::with_name("resolve").arg(Arg::with_name("url").required(true))
}

pub fn exec(matches: &ArgMatches) {
    let url = matches.value_of("url").unwrap();

    let url = match Url::parse(url) {
        Ok(u) => u,
        Err(e) => {
            println!("Invalid url, got error: {:#?}", e);
            return;
        }
    };

    let mut rt = match tokio::runtime::Runtime::new() {
        Ok(rt) => rt,
        Err(e) => {
            println!("Failed to init runtime, got: {:#?}", e);
            return;
        }
    };

    match Link::from(url) {
        Link::Spaste(url) => {
            let spaste_client = spaste::Client::new();

            let spaste_code = match spaste::Code::from_url(&url) {
                Ok(c) => c,
                Err(e) => {
                    println!(
                        "Failed to extract spaste code from url, got error: {:#?}",
                        e
                    );
                    return;
                }
            };

            println!("Solving Captcha...");
            let page_data =
                match rt.block_on(spaste_client.get(&spaste_code, |mut captcha| async move {
                    println!("Unable to solve captcha...");

                    match &mut captcha {
                        Captcha::Homebrew(..) => {
                            // This captcha is solved on creation, do nothing
                        }
                        Captcha::SolveMedia(captcha) => {
                            let img = captcha.image.as_ref().ok_or_else(|| {
                                GenericError::from("Missing SolveMedia Captcha Image")
                            })?;

                            println!("Saving challenge to \"./img.gif\"...");
                            tokio::fs::write("./img.gif", img).await?;

                            println!("Enter the solution: ");
                            let input = read_input();
                            println!();

                            captcha.answer = Some(input);
                        }
                        Captcha::HCaptcha(captcha) => {
                            let path = Path::new("testing");

                            let hcaptcha = captcha
                                .captcha
                                .as_ref()
                                .ok_or_else(|| GenericError::from("Missing HCaptcha"))?;

                            let question = hcaptcha
                                .get_question()
                                .ok_or_else(|| GenericError::from("Missing HCaptcha Question"))?;

                            println!("Saving challenge data to \"testing\"...");
                            match tokio::fs::create_dir(path).await {
                                Ok(_) => {}
                                Err(e) if e.kind() == std::io::ErrorKind::AlreadyExists => {}
                                Err(e) => return Err(e.into()),
                            }

                            match &hcaptcha.request_type {
                                RequestType::ImageLabelBinary => {
                                    for (i, img) in captcha.examples.iter().enumerate() {
                                        let file_path = path.join(format!("example_{}.jpg", i));
                                        tokio::fs::write(file_path, img).await?;
                                    }
                                }
                                RequestType::ImageLabelMultipleChoice => {
                                    let answer_set = hcaptcha
                                        .requester_restricted_answer_set
                                        .as_ref()
                                        .ok_or_else(|| {
                                            GenericError::from("Missing HCaptcha Answer Set")
                                        })?;

                                    let answer_set: Vec<_> = answer_set.keys().collect();

                                    println!("Answers: ");
                                    for (i, key) in answer_set.iter().enumerate() {
                                        println!("{}) {}", i, key);
                                    }
                                }
                                RequestType::Other(s) => {
                                    return Err(GenericError::from(format!(
                                        "Unknown HCaptcha Challenge Type: {}",
                                        s
                                    ))
                                    .into())
                                }
                            }

                            for (i, task) in captcha.images.iter().enumerate() {
                                let file_path = path.join(format!("image_{}.jpg", i));
                                tokio::fs::write(file_path, task).await?;
                            }

                            println!("{}", question);

                            let user_response = read_input();
                            let mut answers = user_response.split(',').map(|el| el.trim());

                            let answers =
                                hcaptcha.generate_answers(&mut answers).ok_or_else(|| {
                                    GenericError::from("Failed to generate HCaptcha Answers")
                                })?;

                            captcha.answers = Some(answers);
                        }
                    }

                    Ok(captcha)
                })) {
                    Ok(data) => data,
                    Err(e) => {
                        println!("Failed to get spaste data, got error: {:#?}", e);
                        return;
                    }
                };

            let doc = Document::from(page_data.as_str());
            let mut links_page = match LinksPage::from_doc(&doc) {
                Ok(p) => p,
                Err(e) => {
                    println!("Failed to parse links page, got error: {:#?}", e);
                    return;
                }
            };

            let num_links = links_page.links().len();

            println!("Resolving {} Redirect URLs...", num_links);
            let now = Instant::now();
            for (i, new_link) in rt
                .block_on(resolve_many_par(links_page.links().iter()))
                .into_iter()
                .enumerate()
            {
                match (links_page.get_link_mut(i), new_link) {
                    (Some(old), Ok(mut new)) => {
                        std::mem::swap(old, &mut new);
                    }
                    (Some(old), Err(e)) => {
                        println!("Failed to resolve '{}', got error: {:#?}", old, e);
                    }
                    (None, _) => {
                        panic!("Links page internal indexing is messed up. This is a bug.");
                    }
                }
            }

            let time = now.elapsed().as_nanos();
            let seconds = time as f64 / 1_000_000_000.0;
            let links_per_second = num_links as f64 / seconds;

            println!(
                "Resolved all links in {} second(s) [{} links/sec].",
                seconds, links_per_second
            );
            println!("Done!");

            println!();
            println!();
            println!("{}", links_page.render());
        }
        Link::Shorte(url) => {
            let shorte_client = shorte::Client::new();
            let link = match rt.block_on(shorte_client.resolve(&url)) {
                Ok(l) => l,
                Err(e) => {
                    println!("Failed to resolve '{}', got error: {:#?}", url.as_url(), e);
                    return;
                }
            };

            println!("{}", link);
        }
        Link::Ouo(url) => {
            let ouo_client = ouo::Client::new();
            let link = match rt.block_on(ouo_client.resolve(&url)) {
                Ok(l) => l,
                Err(e) => {
                    println!("Failed to resolve '{}', got error: {:#?}", url.as_url(), e);
                    return;
                }
            };

            println!("{}", link);
        }
        Link::Unknown(url) => {
            println!("Error, url {} is unknown", url);
        }
    }
}
