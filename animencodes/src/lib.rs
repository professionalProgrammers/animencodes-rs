mod anime_entry;
#[cfg(feature = "cli")]
pub mod cli;
mod client;
mod error;
pub mod links_page;
mod main_page;
pub mod parser;

pub use crate::{
    anime_entry::*,
    client::Client,
    error::{
        AnimeError,
        AnimeResult,
    },
    links_page::LinksPage,
    main_page::{
        AnimeEntryError,
        MainPage,
        MainPageError,
        NewAnimeEntry,
    },
};
pub use reqwest::Url;
pub use select::document::Document;
use std::{
    borrow::Borrow,
    str::FromStr,
};

/// Resolve many "short-links" in parallel.
pub async fn resolve_many_par<I: Iterator<Item = T>, T: Borrow<Link>>(
    iter: I,
) -> Vec<Result<Link, ResolveManyError>> {
    let shorte_client = shorte::Client::new();
    let ouo_client = ouo::Client::new();
    futures::future::join_all(iter.map(|link| {
        let shorte_client = &shorte_client;
        let ouo_client = &ouo_client;
        async move {
            match link.borrow() {
                Link::Shorte(u) => shorte_client
                    .resolve(u)
                    .await
                    .map_err(ResolveManyError::Shorte)
                    .and_then(|s| Link::from_str(&s).map_err(ResolveManyError::Url)),
                Link::Ouo(u) => ouo_client
                    .resolve(&u)
                    .await
                    .map(Link::from)
                    .map_err(ResolveManyError::Ouo),
                l => Err(ResolveManyError::MissingResolver(l.clone())),
            }
        }
    }))
    .await
}

#[derive(Debug)]
pub enum ResolveManyError {
    Url(url::ParseError),
    Shorte(shorte::Error),
    Ouo(ouo::Error),
    MissingResolver(Link),
}
