mod new_anime_entry;

pub use self::new_anime_entry::NewAnimeEntry;
use crate::{
    parser::{
        anime_parser::OwnedParseError as OwnedAnimeNameParseError,
        AnimeNameParseError,
    },
    AnimeEntry,
    AnimeType,
};
use reqwest::Url;
use select::{
    document::Document,
    node::Node,
    predicate::{
        And,
        Attr,
        Class,
        Name,
        Not,
        Predicate,
        Text,
    },
};

#[derive(Debug)]
pub enum MainPageError {
    MissingNew,
    Missing(AnimeType),
    InvalidAnimeEntry(AnimeEntryError),
}

impl From<AnimeEntryError> for MainPageError {
    fn from(e: AnimeEntryError) -> Self {
        Self::InvalidAnimeEntry(e)
    }
}

#[derive(Debug)]
pub enum AnimeEntryError {
    MissingName,
    InvalidName(OwnedAnimeNameParseError),
}

impl<'a> From<AnimeNameParseError<'a>> for AnimeEntryError {
    fn from(e: AnimeNameParseError<'a>) -> Self {
        Self::InvalidName(e.into())
    }
}

#[derive(Clone, Debug)]
pub struct MainPage {
    new_anime: Vec<NewAnimeEntry>,
    dubbed_anime: Vec<AnimeEntry>,
    subbed_anime: Vec<AnimeEntry>,
}

impl MainPage {
    pub fn from_doc(document: &Document) -> Result<Self, MainPageError> {
        let new_anime = document
            .find(And(Class("sidebar"), Class("section")))
            .last()
            .ok_or(MainPageError::MissingNew)?
            .find(Name("ul"))
            .last()
            .ok_or(MainPageError::MissingNew)?
            .find(Name("li"))
            .map(|el| {
                let title = el.find(Text).last()?.as_text()?;
                Some(NewAnimeEntry::from_title(title))
            })
            .collect::<Option<Vec<_>>>()
            .ok_or(MainPageError::MissingNew)?
            .into_iter()
            .flatten()
            .collect();

        let dubbed_anime = parse_entries_from_doc(document, AnimeType::Dub)?;
        let subbed_anime = parse_entries_from_doc(document, AnimeType::Sub)?;

        Ok(MainPage {
            new_anime,
            dubbed_anime,
            subbed_anime,
        })
    }

    pub fn new_anime(&self) -> &[NewAnimeEntry] {
        &self.new_anime
    }

    pub fn dubbed_entries(&self) -> &[AnimeEntry] {
        &self.dubbed_anime
    }

    pub fn subbed_entries(&self) -> &[AnimeEntry] {
        &self.subbed_anime
    }
}

fn get_div_predicate(anime_type: AnimeType) -> impl Predicate {
    match anime_type {
        AnimeType::Dub => And(Class("tabcontent"), Attr("id", "Dual Audio")),
        AnimeType::Sub => And(Class("tabcontent"), Attr("id", "Eng Sub")),
    }
}

fn parse_entries_from_doc(
    doc: &Document,
    anime_type: AnimeType,
) -> Result<Vec<AnimeEntry>, MainPageError> {
    Ok(doc
        .find(get_div_predicate(anime_type))
        .last()
        .ok_or(MainPageError::Missing(anime_type))?
        .find(And(Name("tr"), Not(Class("header"))))
        .map(|el| anime_entry_from_node(el, anime_type))
        .collect::<Result<Vec<_>, _>>()?)
}

fn anime_entry_from_node(el: Node, anime_type: AnimeType) -> Result<AnimeEntry, AnimeEntryError> {
    let name = el
        .find(Name("td"))
        .next()
        .ok_or(AnimeEntryError::MissingName)?
        .find(Text)
        .last()
        .ok_or(AnimeEntryError::MissingName)?
        .as_text()
        .ok_or(AnimeEntryError::MissingName)?;

    let link = el
        .find(Name("a"))
        .last()
        .and_then(|el| Url::parse(el.attr("href")?).ok())
        .map(Into::into);

    let mut entry = AnimeEntry::from_name(name)?;
    entry.link = link;
    entry.anime_type = Some(anime_type);

    Ok(entry)
}
