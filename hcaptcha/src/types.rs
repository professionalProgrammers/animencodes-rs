pub mod captcha;
pub mod site_config;

pub use self::{
    captcha::Captcha,
    site_config::SiteConfig,
};

fn epoch_millis() -> u128 {
    use std::time::{
        SystemTime,
        UNIX_EPOCH,
    };

    SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_millis()
}

/// Response type for getting a pass
#[derive(Debug, serde::Deserialize)]
pub struct CheckCaptchaResponse {
    pub expiration: Option<u64>,

    #[serde(rename = "generated_pass_UUID")]
    pub generated_pass_uuid: Option<String>,

    /// Whether this request suceeded
    pub pass: bool,
}

// Looks like there might be different kinds of motion data, this corresponds with the check_captcha submission
#[derive(Debug, serde::Serialize)]
pub struct MotionData {
    #[serde(rename = "st")]
    pub start_time: u128,

    dct: u64,

    // TODO: Represent mouse motions better
    #[serde(rename = "mm")]
    mouse_move: Vec<[u64; 3]>,

    #[serde(rename = "md")]
    mouse_down: Vec<[u64; 3]>,

    #[serde(rename = "mu")]
    mouse_up: Vec<[u64; 3]>,
}

impl MotionData {
    pub fn new() -> Self {
        let time = epoch_millis();
        MotionData {
            start_time: time,
            dct: time as u64,
            mouse_move: Vec::new(),
            mouse_down: Vec::new(),
            mouse_up: Vec::new(),
        }
    }
}

impl Default for MotionData {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(Debug, Clone)]
pub struct SiteKey(String);

impl SiteKey {
    pub fn new(key: &str) -> Self {
        Self(key.into())
    }

    pub fn as_str(&self) -> &str {
        self.0.as_str()
    }
}
