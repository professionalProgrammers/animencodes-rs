use crate::{
    types::{
        captcha::Captcha,
        site_config::SiteConfig,
        MotionData,
        SiteKey,
    },
    CheckCaptchaResponse,
    HumanError,
    HumanResult,
};
use bytes::Bytes;
use chrono::{
    Datelike,
    Timelike,
};

// IE UA
const USER_AGENT_STR: &str = "Mozilla/5.0 (Windows NT 10.0; Trident/7.0; rv:11.0) like Gecko";

fn gen_nonce(site_key: &SiteKey) -> String {
    let s = 2; // TODO: Find out if this ever changes
    let time = {
        let now = chrono::Utc::now();

        // "2020 06 26 01 38 58"
        format!(
            "{}{:02}{:02}{:02}{:02}{:02}",
            now.year(),
            now.month(),
            now.day(),
            now.hour(),
            now.minute(),
            now.second()
        )
    };
    let hash = 2; // TODO: Port hash func from js
    format!("1:{}:{}:{}::{}", s, time, site_key.as_str(), hash)
}

pub struct Client {
    client: reqwest::Client,
}

impl Client {
    pub fn new() -> Self {
        let client = reqwest::ClientBuilder::new()
            .user_agent(USER_AGENT_STR)
            .build()
            .unwrap();
        Client { client }
    }

    pub async fn get_bytes(&self, url: &str) -> HumanResult<Bytes> {
        let res = self.client.get(url).send().await?;

        let status = res.status();

        if !status.is_success() {
            return Err(HumanError::InvalidStatus(status));
        }

        let bytes = res.bytes().await?;

        Ok(bytes)
    }

    /// Get the site config
    pub async fn check_site_config(
        &self,
        host: &str,
        site_key: &SiteKey,
    ) -> HumanResult<SiteConfig> {
        let url = format!(
            "https://hcaptcha.com/checksiteconfig?host={}&sitekey={}&sc=1&swa=0",
            host,
            site_key.as_str()
        );

        let bytes = self.get_bytes(&url).await?;

        Ok(serde_json::from_slice(&bytes)?)
    }

    /// Get the captcha
    pub async fn get_captcha(
        &self,
        site_key: &SiteKey,
        host: &str,
        site_config: &SiteConfig,
    ) -> HumanResult<Captcha> {
        let config_payload = serde_json::to_string(&site_config.config)?;

        let res = self
            .client
            .post("https://hcaptcha.com/getcaptcha")
            .form(&[
                ("sitekey", site_key.as_str()),
                ("host", host),
                ("hl", "en"),
                ("motionData", "{}"), // TODO: Pass motion data
                ("n", &gen_nonce(&site_key)),
                ("c", config_payload.as_str()), // Copied from site_check
            ])
            .send()
            .await?;

        let status = res.status();
        if !status.is_success() {
            return Err(HumanError::InvalidStatus(status));
        }

        let text = res.text().await?;
        let json = serde_json::from_str(&text)?;

        Ok(json)
    }

    pub async fn check_captcha(
        &self,
        captcha: &Captcha,
        host: &str,
        site_key: &SiteKey,
        site_config: &SiteConfig,
        answers: &serde_json::Value,
        motion_data: &MotionData,
    ) -> HumanResult<CheckCaptchaResponse> {
        let url = format!("https://hcaptcha.com/checkcaptcha/{}", captcha.key);

        let motion_data = serde_json::to_string(&motion_data)?;
        let config_payload = serde_json::to_string(&site_config.config)?;

        let payload = serde_json::json!({
            "answers": answers,
            "c": config_payload.as_str(),
            "job_mode": captcha.request_type,
            "motionData": &motion_data,
            "n": &gen_nonce(&site_key),
            "serverdomain": host,
            "sitekey": site_key.as_str(),
        });

        let res = self.client.post(&url).json(&payload).send().await?;

        let status = res.status();
        if !status.is_success() {
            return Err(HumanError::InvalidStatus(status));
        }

        let bytes = res.bytes().await?;

        Ok(serde_json::from_slice(&bytes)?)
    }
}

impl Default for Client {
    fn default() -> Self {
        Client::new()
    }
}
