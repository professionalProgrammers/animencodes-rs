use std::collections::HashMap;
use url::Url;

#[derive(serde::Deserialize, Debug, Clone)]
pub struct Captcha {
    #[serde(rename = "bypass-message")]
    pub bypass_message: String,

    pub challenge_uri: Url,
    pub key: String,
    pub request_config: Option<RequestConfig>,
    pub request_type: RequestType,
    pub requester_restricted_answer_set: Option<HashMap<String, HashMap<String, String>>>,
    pub requester_question: HashMap<String, String>,
    pub requester_question_example: Option<Vec<Url>>,
    pub tasklist: Vec<Task>,
}

impl Captcha {
    pub fn get_question(&self) -> Option<&str> {
        self.requester_question.values().next().map(|s| s.as_str())
    }

    pub fn generate_answers<'a, I: Iterator<Item = &'a str>>(
        &self,
        answers: I,
    ) -> Option<serde_json::Value> {
        let mut ret = serde_json::map::Map::new();

        match self.request_type {
            RequestType::ImageLabelBinary => {
                let answers = answers
                    .map(|s| s.parse::<usize>().ok())
                    .collect::<Option<Vec<_>>>()?;

                for (i, task) in self.tasklist.iter().enumerate() {
                    let value = if answers.contains(&i) {
                        "true"
                    } else {
                        "false"
                    };

                    ret.insert(task.task_key.to_string(), value.into());
                }
            }
            RequestType::ImageLabelMultipleChoice => {
                for (task, answer) in self.tasklist.iter().zip(answers) {
                    ret.insert(task.task_key.to_string(), (&[answer][..]).into());
                }
            }
            _ => return None,
        }

        Some(ret.into())
    }
}

#[derive(serde::Deserialize, Debug, Clone)]
pub struct RequestConfig {
    pub max_points: Option<serde_json::Value>,
    pub max_shapes_per_image: Option<serde_json::Value>,
    pub min_points: Option<serde_json::Value>,
    pub min_shapes_per_image: Option<serde_json::Value>,
    pub minimum_selection_area_per_shape: Option<serde_json::Value>,
    pub multiple_choice_max_choices: u32,
    pub multiple_choice_min_choices: u32,
    pub restrict_to_coords: Option<serde_json::Value>,
    pub shape_type: Option<serde_json::Value>,
    pub version: u32,
}

#[derive(serde::Deserialize, Debug, Clone)]
pub struct Task {
    pub datapoint_uri: Url,
    pub task_key: String,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, serde::Deserialize, serde::Serialize)]
#[serde(from = "&str", into = "String")]
pub enum RequestType {
    ImageLabelBinary,
    ImageLabelMultipleChoice,
    Other(String),
}

impl From<&'_ str> for RequestType {
    fn from(s: &str) -> RequestType {
        match s {
            "image_label_binary" => RequestType::ImageLabelBinary,
            "image_label_multiple_choice" => RequestType::ImageLabelMultipleChoice,
            s => RequestType::Other(s.into()),
        }
    }
}

impl Into<String> for RequestType {
    fn into(self) -> String {
        match self {
            RequestType::ImageLabelBinary => "image_label_binary".into(),
            RequestType::ImageLabelMultipleChoice => "image_label_multiple_choice".into(),
            RequestType::Other(s) => s,
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    const SAMPLE_1: &str = include_str!("../../test_data/captcha_1.json");
    const SAMPLE_2: &str = include_str!("../../test_data/captcha_2.json");

    #[test]
    fn parse_captcha_1() {
        serde_json::from_str::<Captcha>(SAMPLE_1).unwrap();
    }

    #[test]
    fn parse_captcha_2() {
        serde_json::from_str::<Captcha>(SAMPLE_2).unwrap();
    }
}
