#[derive(serde::Deserialize, serde::Serialize, Debug, Clone)]
pub struct SiteConfig {
    #[serde(rename = "c")]
    pub config: Config,

    pub pass: bool,
}

#[derive(serde::Deserialize, serde::Serialize, Debug, Clone)]
pub struct Config {
    #[serde(rename = "type")]
    pub kind: String,

    /// A json web token for the session
    pub req: String,
}

#[cfg(test)]
mod test {
    use super::*;

    const SAMPLE_1: &str = include_str!("../../test_data/site_config.json");

    #[test]
    fn parse_site_config() {
        serde_json::from_str::<SiteConfig>(&SAMPLE_1).unwrap();
    }
}
