pub mod client;
pub mod types;

pub use crate::{
    client::Client,
    types::{
        captcha::RequestType,
        Captcha,
        CheckCaptchaResponse,
        MotionData,
        SiteConfig,
        SiteKey,
    },
};

/// Result type
pub type HumanResult<T> = Result<T, HumanError>;

/// Error type
#[derive(Debug, thiserror::Error)]
pub enum HumanError {
    /// Reqwest HTTP Error
    #[error("{0}")]
    Reqwest(#[from] reqwest::Error),

    /// Invalid HTTP Status
    #[error("{0}")]
    InvalidStatus(reqwest::StatusCode),

    /// Json error
    #[error("{0}")]
    Json(#[from] serde_json::Error),
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::{
        io::{
            stdin,
            stdout,
            Write,
        },
        path::Path,
    };

    fn read_input() -> String {
        let mut s = String::new();

        let _ = stdout().flush();
        stdin()
            .read_line(&mut s)
            .expect("Did not enter a correct string");
        if let Some('\n') = s.chars().next_back() {
            s.pop();
        }
        if let Some('\r') = s.chars().next_back() {
            s.pop();
        }

        s
    }

    #[tokio::test]
    #[ignore]
    async fn it_works() {
        let key = SiteKey::new("2785c22c-80b4-4b1a-bf40-a18261b10a0a");
        let host = "www.spaste.com";

        let client = Client::new();

        let config = client.check_site_config(host, &key).await.unwrap();
        dbg!(&config);

        let captcha = client.get_captcha(&key, &host, &config).await.unwrap();
        dbg!(&captcha);

        let path = Path::new("testing");
        let _ = std::fs::create_dir(path);

        match captcha.request_type {
            RequestType::ImageLabelBinary => {
                for (i, file) in captcha
                    .requester_question_example
                    .as_ref()
                    .unwrap()
                    .iter()
                    .enumerate()
                {
                    let file_path = path.join(format!("example_{}.jpg", i));
                    let data = client.get_bytes(file.as_str()).await.unwrap();
                    std::fs::write(file_path, data).unwrap();
                }
            }
            RequestType::ImageLabelMultipleChoice => {
                println!("Answers: ");
                for (i, key) in captcha
                    .requester_restricted_answer_set
                    .as_ref()
                    .unwrap()
                    .keys()
                    .enumerate()
                {
                    println!("{}) {}", i, key);
                }
            }
            _ => todo!(),
        }

        for (i, task) in captcha.tasklist.iter().enumerate() {
            let file_path = path.join(format!("task_{}.jpg", i));
            let data = client.get_bytes(task.datapoint_uri.as_str()).await.unwrap();
            std::fs::write(file_path, data).unwrap();
        }

        println!("{}", captcha.get_question().unwrap());

        let user_response = read_input();
        let mut answers = user_response.split(',').map(|el| el.trim());

        let answers = captcha.generate_answers(&mut answers).unwrap();

        dbg!(&answers);

        let motion_data = MotionData::new();

        let res = client
            .check_captcha(&captcha, host, &key, &config, &answers, &motion_data)
            .await
            .unwrap();

        dbg!(res);
    }
}
