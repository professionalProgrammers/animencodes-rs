use crate::client::CaptchaPageDataError;
use std::error::Error;

pub type PasteResult<T> = Result<T, PasteError>;

#[derive(Debug)]
pub enum PasteError {
    Reqwest(reqwest::Error),
    InvalidStatus(reqwest::StatusCode),
    InvalidRedirect(url::Url),
    Io(std::io::Error),
    InvalidCaptchaPage(CaptchaPageDataError),
    SolveMedia(solve_media::SolveError),
    HCaptcha(hcaptcha::HumanError),

    MissingSessionCookie,

    /// Failed Captcha challenge
    FailedChallenge,

    /// Error from the captcha solving callback future
    UserError(UserError),

    /// Generic String Error
    GenericError(String),
}

#[derive(Debug)]
pub struct UserError(pub Box<dyn Error + Send>);

impl<T: Error + Send + 'static> From<T> for UserError {
    fn from(e: T) -> UserError {
        UserError(Box::new(e))
    }
}

impl From<UserError> for PasteError {
    fn from(e: UserError) -> Self {
        PasteError::UserError(e)
    }
}

impl From<CaptchaPageDataError> for PasteError {
    fn from(e: CaptchaPageDataError) -> Self {
        Self::InvalidCaptchaPage(e)
    }
}

impl From<std::io::Error> for PasteError {
    fn from(e: std::io::Error) -> Self {
        Self::Io(e)
    }
}

impl From<reqwest::Error> for PasteError {
    fn from(e: reqwest::Error) -> Self {
        Self::Reqwest(e)
    }
}

impl From<solve_media::SolveError> for PasteError {
    fn from(e: solve_media::SolveError) -> Self {
        Self::SolveMedia(e)
    }
}

impl From<hcaptcha::HumanError> for PasteError {
    fn from(e: hcaptcha::HumanError) -> Self {
        Self::HCaptcha(e)
    }
}
