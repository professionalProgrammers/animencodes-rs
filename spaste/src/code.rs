use std::{
    convert::{
        TryFrom,
        TryInto,
    },
    str::FromStr,
};
use url::Url;

#[derive(Debug)]
pub enum CodeParseError {
    NonAsciiChar(usize, char),
    NonAlphaNumericChar(usize, char),
}

#[derive(Debug)]
pub enum CodeParseUrlError {
    Url(url::ParseError),
    Parse(CodeParseError),
    MissingCode,
}

impl From<url::ParseError> for CodeParseUrlError {
    fn from(e: url::ParseError) -> Self {
        Self::Url(e)
    }
}

impl From<CodeParseError> for CodeParseUrlError {
    fn from(e: CodeParseError) -> Self {
        Self::Parse(e)
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
pub struct Code(String);

impl Code {
    pub fn get_url(&self) -> Url {
        self.into()
    }

    pub fn as_str(&self) -> &str {
        &self.0
    }

    pub fn verify(s: &str) -> Result<(), CodeParseError> {
        for (i, c) in s.chars().enumerate() {
            if !c.is_ascii() {
                return Err(CodeParseError::NonAsciiChar(i, c));
            }

            if !(c as u8).is_ascii_alphanumeric() {
                return Err(CodeParseError::NonAlphaNumericChar(i, c));
            }
        }

        Ok(())
    }

    pub fn from_code_str(c: &str) -> Result<Self, CodeParseError> {
        c.try_into()
    }

    pub fn from_url(url: &Url) -> Result<Self, CodeParseUrlError> {
        url.try_into()
    }

    pub fn from_url_str(url: &str) -> Result<Self, CodeParseUrlError> {
        Url::parse(url)?.try_into()
    }
}

impl TryFrom<String> for Code {
    type Error = CodeParseError;

    fn try_from(s: String) -> Result<Self, Self::Error> {
        Code::verify(&s)?;
        Ok(Code(s))
    }
}

impl<'a> TryFrom<&'a str> for Code {
    type Error = <Self as TryFrom<String>>::Error;

    fn try_from(s: &'a str) -> Result<Self, Self::Error> {
        Code::verify(&s)?;
        Ok(Code(String::from(s)))
    }
}

impl TryFrom<Url> for Code {
    type Error = CodeParseUrlError;

    fn try_from(url: Url) -> Result<Self, Self::Error> {
        (&url).try_into()
    }
}

impl<'a> TryFrom<&'a Url> for Code {
    type Error = CodeParseUrlError;

    fn try_from(url: &'a Url) -> Result<Self, Self::Error> {
        let s = url
            .query_pairs()
            .filter(|(k, _v)| k == "c")
            .map(|(_k, v)| v)
            .last()
            .ok_or(Self::Error::MissingCode)?;
        Ok(s.parse()?)
    }
}

impl Into<Url> for Code {
    fn into(self) -> Url {
        (&self).into()
    }
}

impl<'a> Into<Url> for &'a Code {
    fn into(self) -> Url {
        Url::parse_with_params(
            "https://www.spaste.com/site/checkPasteUrl",
            &[("c", &self.0)],
        )
        .unwrap()
    }
}

impl FromStr for Code {
    type Err = <Self as TryFrom<&'static str>>::Error;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        s.try_into()
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn code_to_url() {
        let code: Code = "i9H6Mf1245".parse().unwrap();
        assert_eq!(
            code.get_url().as_str(),
            "https://www.spaste.com/site/checkPasteUrl?c=i9H6Mf1245"
        );
    }

    #[test]
    fn url_to_code() {
        let url = Url::parse("https://www.spaste.com/p/?c=i9H6Mf1245").unwrap();
        let code: Code = url.try_into().unwrap();
        assert_eq!(code.as_str(), "i9H6Mf1245");
    }
}
