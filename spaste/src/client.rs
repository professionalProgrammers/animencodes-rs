pub(crate) mod captcha_page_data;
mod form_data;

pub use self::captcha_page_data::CaptchaPageDataError;
use self::{
    captcha_page_data::CaptchaPageData,
    form_data::{
        HomebrewFormData,
        SolveMediaFormData,
    },
};
use crate::{
    error::UserError,
    Captcha,
    Code,
    PasteError,
    PasteResult,
};
use bytes::buf::BufExt;
use select::document::Document;
use std::future::Future;

const USER_AGENT_STR: &str = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3721.3 Safari/537.36";

#[derive(Default)]
pub struct Client {
    client: reqwest::Client,

    pub(crate) solve_media_client: solve_media::Client,
    pub(crate) hcaptcha_client: hcaptcha::Client,
}

impl Client {
    pub fn new() -> Self {
        Default::default()
    }

    pub async fn get<T, F>(&self, code: &Code, cb: T) -> PasteResult<String>
    where
        T: Fn(Captcha) -> F,
        F: Future<Output = Result<Captcha, UserError>>,
    {
        let code_url = code.get_url();

        let res = self
            .client
            .get(code_url.as_str())
            .header(reqwest::header::USER_AGENT, USER_AGENT_STR)
            .send()
            .await?;

        let status = res.status();
        if !status.is_success() {
            return Err(PasteError::InvalidStatus(status));
        }

        let session_cookie = res
            .cookies()
            .find(|c| c.name() == "PHPSESSID")
            .map(|c| format!("{}={}", c.name(), c.value()))
            .ok_or(PasteError::MissingSessionCookie)?;

        let CaptchaPageData {
            token,
            bounce,
            mut captcha,
        } = {
            let doc = Document::from_read(res.bytes().await?.reader())?;
            CaptchaPageData::from_doc(&doc)?
        };

        let req = self
            .client
            .post(code_url.as_str())
            .header(reqwest::header::USER_AGENT, USER_AGENT_STR)
            .header(reqwest::header::COOKIE, session_cookie);

        captcha.populate(&self).await?;
        let captcha = cb(captcha).await?;

        let res = match captcha {
            Captcha::HCaptcha(captcha) => {
                let host = "www.spaste.com";
                let motion_data = hcaptcha::MotionData::new();

                let site_key = captcha.site_key;
                let answers = captcha
                    .answers
                    .ok_or_else(|| PasteError::GenericError("Missing HCaptcha Answers".into()))?;

                let site_config = captcha.site_config.ok_or_else(|| {
                    PasteError::GenericError("Missing HCaptcha site config".into())
                })?;

                let captcha = captcha
                    .captcha
                    .ok_or_else(|| PasteError::GenericError("Missing HCaptcha Captcha".into()))?;

                let response = {
                    let response = self
                        .hcaptcha_client
                        .check_captcha(
                            &captcha,
                            host,
                            &site_key,
                            &site_config,
                            &answers,
                            &motion_data,
                        )
                        .await?;

                    if !response.pass {
                        return Err(PasteError::FailedChallenge);
                    }

                    response
                        .generated_pass_uuid
                        .ok_or(PasteError::FailedChallenge)?
                };

                req.form(&[
                    ("g-recaptcha-response", response.as_str()),
                    ("h-captcha-response", response.as_str()),
                    ("turn", "5"),
                    ("bounce", &bounce),
                    ("token", &token),
                    ("detector", "http://www.spaste.com/site/checkBlockedDotCom"),
                    ("pasteUrlForm[submit]", "submit"),
                ])
            }
            Captcha::SolveMedia(captcha) => {
                let solve_media_captcha = captcha
                    .captcha
                    .as_ref()
                    .ok_or_else(|| PasteError::GenericError("Missing SolveMedia Captcha".into()))?;

                let answer = captcha
                    .answer
                    .as_ref()
                    .ok_or_else(|| PasteError::GenericError("Missing SolveMedia Answer".into()))?;

                self.solve_media_client
                    .submit(&captcha.key, solve_media_captcha, &answer)
                    .await?;

                let data = SolveMediaFormData::from_captcha(&solve_media_captcha, &bounce, &token);
                req.form(&data)
            }
            Captcha::Homebrew(ref captcha) => {
                let data = HomebrewFormData::from_captcha(captcha, &bounce, &token);
                req.form(&data)
            }
        }
        .send()
        .await?;

        let status = res.status();
        if !status.is_success() {
            return Err(PasteError::InvalidStatus(status));
        }

        // TODO: use Url::parse_with_params
        let res_url = res.url();
        if res_url.as_str() != format!("https://www.spaste.com/p?c={}", code.as_str()) {
            return Err(PasteError::InvalidRedirect(res_url.clone()));
        }

        Ok(res.text().await?)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    fn assert_send<T: Send>(_t: T) {}

    #[test]
    fn static_assertions() {
        let code: Code = "i9H6Mf1245".parse().unwrap();
        let client = Client::new();
        let get = client.get(&code, |c| async { Ok(c) });

        assert_send(get);
        assert_send(client);
    }
}
