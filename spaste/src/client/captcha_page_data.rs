use crate::Captcha;
use select::{
    document::Document,
    predicate::{
        And,
        Attr,
        Name,
    },
};

#[derive(Debug)]
pub enum CaptchaPageDataError {
    MissingToken,
    MissingBounce,
    MissingCaptcha,
}

fn get_input_value_by_name<'a>(doc: &'a Document, name: &str) -> Option<&'a str> {
    doc.find(And(Name("input"), Attr("name", name)))
        .last()
        .and_then(|el| el.attr("value"))
}

#[derive(Debug)]
pub(crate) struct CaptchaPageData {
    pub token: String,
    pub bounce: String,
    pub captcha: Captcha,
}

impl CaptchaPageData {
    pub fn from_doc(doc: &Document) -> Result<Self, CaptchaPageDataError> {
        let token = get_input_value_by_name(doc, "token")
            .ok_or(CaptchaPageDataError::MissingToken)?
            .to_string();

        let bounce = get_input_value_by_name(doc, "bounce")
            .ok_or(CaptchaPageDataError::MissingBounce)?
            .to_string();

        let captcha = Captcha::from_doc(&doc).ok_or(CaptchaPageDataError::MissingCaptcha)?;

        Ok(CaptchaPageData {
            token,
            bounce,
            captcha,
        })
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn parse_homebrew_captcha_page() {
        let homebrew_page = include_str!("../../tests/homebrew_page.html");
        let doc = Document::from(homebrew_page);
        let page = CaptchaPageData::from_doc(&doc).unwrap();
        assert_eq!(page.token, "af10cc32203cee2e201b906d76703ce9");
        assert_eq!(page.bounce, "148208552");
        let captcha = page.captcha.into_homebrew().unwrap();
        assert_eq!(captcha.hash(), "7e4f47da10da1c7b080ace4517ec44e3");
        assert_eq!(captcha.user_hash(), "831");
    }
}
