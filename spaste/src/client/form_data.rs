use crate::homebrew::Homebrew;
use serde::Serialize;

#[derive(Debug, Serialize)]
pub struct HomebrewFormData<'a> {
    #[serde(rename = "sPasteCaptcha")]
    spaste_captcha: &'a str,
    #[serde(rename = "userEnterHashHere")]
    user_hash: &'a str,
    turn: u8,
    bounce: &'a str,
    token: &'a str,
    detector: &'static str,
    #[serde(rename = "pasteUrlForm[submit]")]
    url: &'static str,
}

impl<'a> HomebrewFormData<'a> {
    pub fn from_captcha(captcha: &'a Homebrew, bounce: &'a str, token: &'a str) -> Self {
        Self {
            spaste_captcha: captcha.hash(),
            user_hash: captcha.user_hash(),
            turn: 2,
            bounce,
            token,
            detector: "http://www.spaste.com/site/checkBlockedDotCom",
            url: "submit",
        }
    }
}

#[derive(Debug, Serialize)]
pub struct SolveMediaFormData<'a> {
    adcopy_challenge: &'a str,
    adcopy_response: &'static str,
    turn: u8,
    bounce: &'a str,
    token: &'a str,
    #[serde(rename = "pasteUrlForm[submit]")]
    url: &'static str,
}

impl<'a> SolveMediaFormData<'a> {
    pub fn from_captcha(
        captcha: &'a solve_media::Captcha,
        bounce: &'a str,
        token: &'a str,
    ) -> Self {
        SolveMediaFormData {
            adcopy_challenge: captcha.code(),
            adcopy_response: "manual_challenge",
            turn: 1,
            bounce,
            token,
            url: "submit",
        }
    }
}
