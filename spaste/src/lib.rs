mod captcha;
mod client;
pub mod code;
mod error;
mod homebrew;

pub use crate::{
    captcha::Captcha,
    client::{
        CaptchaPageDataError,
        Client,
    },
    code::Code,
    error::{
        PasteError,
        PasteResult,
    },
    homebrew::Homebrew,
};
pub use solve_media::SolveError;
pub use url::Url;
