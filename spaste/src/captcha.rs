use crate::{
    client::Client,
    error::PasteResult,
    Homebrew,
};
use bytes::Bytes;
use select::{
    document::Document,
    predicate::{
        And,
        Attr,
        Class,
        Name,
        Not,
        Text,
    },
};
use url::Url;

pub const HOST: &str = "www.spaste.com";

/// An enum of all possible Captchas for spaste
#[derive(Debug, Clone)]
pub enum Captcha {
    HCaptcha(HCaptcha),
    SolveMedia(SolveMedia),
    Homebrew(Homebrew),
}

impl Captcha {
    /// Returns true if this is a hcaptcha
    pub fn is_hcaptcha(&self) -> bool {
        matches!(self, Self::HCaptcha(..))
    }

    /// Returns true if this is a solve_media captcha
    pub fn is_solve_media(&self) -> bool {
        matches!(self, Self::SolveMedia(..))
    }

    /// Returns true if this is a homebrew captcha
    pub fn is_homebrew(&self) -> bool {
        matches!(self, Self::Homebrew(..))
    }

    /// Populates captchas with all data needed to complete them
    pub async fn populate(&mut self, client: &Client) -> PasteResult<()> {
        match self {
            Captcha::Homebrew(_) => {
                // No population needed
            }
            Captcha::SolveMedia(captcha) => captcha.populate(&client.solve_media_client).await?,
            Captcha::HCaptcha(captcha) => captcha.populate(&client.hcaptcha_client).await?,
        }
        Ok(())
    }

    /// Returns Some(captcha) is this was a homebrew captcha, consuming the object
    pub fn into_homebrew(self) -> Option<Homebrew> {
        match self {
            Self::Homebrew(c) => Some(c),
            _ => None,
        }
    }

    /// Extracts a captcha from a doc
    pub(crate) fn from_doc(doc: &Document) -> Option<Self> {
        if let Some(captcha) = HCaptcha::from_doc(&doc) {
            return Some(Self::HCaptcha(captcha));
        }

        if let Some(captcha) = SolveMedia::from_doc(&doc) {
            return Some(Self::SolveMedia(captcha));
        }

        let maybe_homebrew = doc
            .find(And(
                And(Name("script"), Attr("type", "text/javascript")),
                Not(Attr("src", ())),
            ))
            .filter_map(|el| el.find(Text).last())
            .filter_map(|el| el.as_text())
            .filter_map(|el| Homebrew::from_script_str(el).ok())
            .last();

        if let Some(captcha) = maybe_homebrew {
            return Some(Self::Homebrew(captcha));
        }

        None
    }
}

#[derive(Debug, Clone)]
pub struct SolveMedia {
    pub key: solve_media::CaptchaKey,
    pub captcha: Option<solve_media::Captcha>,
    pub image: Option<Bytes>,
    pub answer: Option<String>,
}

impl SolveMedia {
    pub fn from_doc(doc: &Document) -> Option<Self> {
        let el = doc.find(Name("noscript")).last()?;
        let contents = el.find(Text).last()?.as_text()?;
        let noscript = Document::from(contents);
        let captcha_url = noscript
            .find(Name("iframe"))
            .last()
            .and_then(|el| el.attr("src"))?;

        let captcha_url = Url::parse(captcha_url).ok()?;
        let key = solve_media::CaptchaKey::from_url(&captcha_url).ok()?;

        Some(SolveMedia {
            key,
            captcha: None,
            image: None,
            answer: None,
        })
    }

    pub async fn populate(&mut self, client: &solve_media::Client) -> PasteResult<()> {
        let captcha = client.get_captcha(&self.key).await?;
        let image = client.get_challenge_bytes(&captcha).await?;

        self.captcha = Some(captcha);
        self.image = Some(image);

        Ok(())
    }
}

#[derive(Debug, Clone)]
pub struct HCaptcha {
    pub site_key: hcaptcha::SiteKey,
    pub site_config: Option<hcaptcha::SiteConfig>,
    pub captcha: Option<hcaptcha::Captcha>,

    pub examples: Vec<Bytes>,
    pub images: Vec<Bytes>,

    pub answers: Option<serde_json::Value>,
}

impl HCaptcha {
    pub fn from_doc(doc: &Document) -> Option<Self> {
        let site_key = doc
            .find(Class("h-captcha"))
            .next()
            .and_then(|el| el.attr("data-sitekey"))
            .map(hcaptcha::SiteKey::new)?;

        Some(HCaptcha {
            site_key,
            site_config: None,
            captcha: None,

            examples: Vec::new(),
            images: Vec::new(),

            answers: None,
        })
    }

    pub async fn populate(&mut self, client: &hcaptcha::Client) -> PasteResult<()> {
        let site_config = client.check_site_config(HOST, &self.site_key).await?;
        let captcha = client
            .get_captcha(&self.site_key, HOST, &site_config)
            .await?;

        let examples = futures::future::join_all(
            captcha
                .requester_question_example
                .iter()
                .flatten()
                .map(|file| async move { client.get_bytes(file.as_str()).await }),
        )
        .await;
        let examples = examples.into_iter().collect::<Result<_, _>>()?;

        let images = futures::future::join_all(
            captcha
                .tasklist
                .iter()
                .map(|task| async move { client.get_bytes(task.datapoint_uri.as_str()).await }),
        )
        .await;
        let images = images.into_iter().collect::<Result<_, _>>()?;

        self.site_config = Some(site_config);
        self.captcha = Some(captcha);
        self.examples = examples;
        self.images = images;
        Ok(())
    }
}
