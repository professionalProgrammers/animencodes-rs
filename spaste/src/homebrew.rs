use ducc::{
    Ducc,
    FromValue,
    ToValue,
};

const JS_PATCHES: &str = include_str!("js_patches.js");

#[derive(Debug)]
pub enum HomebrewError {
    Ducc(SendDuccError),
    MissingHash,
    MissingQuestion,
    MissingAnswer,
    FailedSolve {
        hash: String,
        question: Vec<String>,
        answer: Vec<String>,
    },
}

impl From<ducc::Error> for HomebrewError {
    fn from(e: ducc::Error) -> Self {
        HomebrewError::Ducc(e.into())
    }
}

#[derive(Debug, Clone)]
pub struct Homebrew {
    hash: String,
    user_hash: String,
}

impl Homebrew {
    pub fn hash(&self) -> &str {
        &self.hash
    }

    pub fn user_hash(&self) -> &str {
        &self.user_hash
    }
}

impl Homebrew {
    pub(crate) fn from_script_str(data: &str) -> Result<Self, HomebrewError> {
        let ducc = Ducc::new();
        ducc.exec(JS_PATCHES, Some("js_patches.js"), Default::default())?;
        ducc.exec(data, Some("captcha.js"), Default::default())?;

        let hash: String = get_global(&ducc, "myCaptchaHash")?.ok_or(HomebrewError::MissingHash)?;
        let question: Vec<String> =
            get_global(&ducc, "myCaptchaQuestions")?.ok_or(HomebrewError::MissingQuestion)?;
        let answer: Vec<String> =
            get_global(&ducc, "myCaptchaAns")?.ok_or(HomebrewError::MissingAnswer)?;

        match get_answer(&question, &answer) {
            Some(user_hash) => Ok(Self { hash, user_hash }),
            None => Err(HomebrewError::FailedSolve {
                hash,
                question,
                answer,
            }),
        }
    }
}

fn get_global<'a, K: ToValue<'a>, V: FromValue<'a>>(
    ducc: &'a Ducc,
    k: K,
) -> Result<Option<V>, ducc::Error> {
    let v: ducc::Value<'_> = ducc.globals().get(k)?;
    match v {
        ducc::Value::Undefined => Ok(None),
        ducc::Value::Null => Ok(None),
        _ => V::from_value(v, ducc).map(Some),
    }
}

fn get_answer<T: AsRef<str>>(questions: &[T], answers: &[T]) -> Option<String> {
    answers
        .iter()
        .map(|answer| {
            questions
                .iter()
                .position(|el| el.as_ref() == answer.as_ref())
                .and_then(|n| std::char::from_digit(n as u32, 10))
        })
        .collect::<Option<String>>()
}

#[derive(Debug)]
pub struct SendDuccError {
    pub kind: SendDuccErrorKind,
    pub context: Vec<String>,
}

impl From<ducc::Error> for SendDuccError {
    fn from(e: ducc::Error) -> Self {
        Self {
            kind: e.kind.into(),
            context: e.context,
        }
    }
}

#[derive(Debug)]
pub enum SendDuccErrorKind {
    ToJsConversionError {
        from: &'static str,
        to: &'static str,
    },
    FromJsConversionError {
        from: &'static str,
        to: &'static str,
    },
    RuntimeError {
        code: ducc::RuntimeErrorCode,
        name: String,
    },
    RecursiveMutCallback,
    ExternalError,
    NotAFunction,
}

impl From<ducc::ErrorKind> for SendDuccErrorKind {
    fn from(e: ducc::ErrorKind) -> Self {
        match e {
            ducc::ErrorKind::ToJsConversionError { from, to } => {
                SendDuccErrorKind::ToJsConversionError { from, to }
            }
            ducc::ErrorKind::FromJsConversionError { from, to } => {
                SendDuccErrorKind::FromJsConversionError { from, to }
            }
            ducc::ErrorKind::RuntimeError { code, name } => {
                SendDuccErrorKind::RuntimeError { code, name }
            }
            ducc::ErrorKind::RecursiveMutCallback => SendDuccErrorKind::RecursiveMutCallback,
            ducc::ErrorKind::ExternalError(_) => SendDuccErrorKind::ExternalError,
            ducc::ErrorKind::NotAFunction => SendDuccErrorKind::NotAFunction,
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn homebrew_from_script_str() {
        let data = include_str!("../tests/homebrew_script.js");
        let hombrew = Homebrew::from_script_str(&data).unwrap();
        assert_eq!(hombrew.hash(), "4e74052a5c487effe917d24d40c2b8e8");
        assert_eq!(hombrew.user_hash(), "825");
    }
}
