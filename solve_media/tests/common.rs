use solve_media::{
    CaptchaKey,
    Client,
    Url,
};
use tokio::io::{
    AsyncBufReadExt,
    AsyncWriteExt,
    BufReader,
};

#[test]
#[ignore]
pub fn solve() {
    let mut rt = tokio::runtime::Runtime::new().unwrap();
    let url = Url::parse(
        "http://api.solvemedia.com/papi/challenge.noscript?k=Pn6QzlWx5u.xPlsbCl8iNGiQF8kd7ulu",
    )
    .unwrap();

    let key = CaptchaKey::from_url(&url).unwrap();

    let client = Client::new();
    let res = rt
        .block_on(client.solve(&key, |_captcha, challenge| async {
            tokio::fs::write("img.gif", challenge).await.unwrap();
            {
                let mut stdout = tokio::io::stdout();
                stdout
                    .write(b"Enter the answer for './img.gif': ")
                    .await
                    .unwrap();
                stdout.flush().await.unwrap();
            }

            let mut input = String::new();
            BufReader::new(tokio::io::stdin())
                .read_line(&mut input)
                .await
                .unwrap();

            println!();

            let _ = tokio::fs::remove_file("img.gif").await;

            Ok(input)
        }))
        .unwrap();

    dbg!(res);
}
