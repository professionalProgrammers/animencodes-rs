use crate::{
    Captcha,
    CaptchaKey,
    SolveError,
    SolveResult,
};
use bytes::{
    buf::BufExt,
    Bytes,
};
use reqwest::header::{
    HeaderMap,
    HeaderValue,
    USER_AGENT,
};
use select::{
    document::Document,
    predicate::{
        Name,
        Text,
    },
};
use std::future::Future;
use url::Url;

const USER_AGENT_STR: &str = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3721.3 Safari/537.36";

#[derive(Default)]
pub struct Client {
    client: reqwest::Client,
}

impl Client {
    pub fn new() -> Self {
        let mut headers = HeaderMap::new();
        headers.insert(USER_AGENT, HeaderValue::from_static(USER_AGENT_STR));
        let client = reqwest::Client::builder()
            .default_headers(headers)
            .build()
            .unwrap();
        Client { client }
    }

    pub async fn get_captcha(&self, key: &CaptchaKey) -> SolveResult<Captcha> {
        let url = key.get_noscript_url();
        let res = self.client.get(url.as_str()).send().await?;
        let status = res.status();
        if !status.is_success() {
            return Err(SolveError::InvalidStatus(status));
        }
        let doc = Document::from_read(res.bytes().await?.reader())?;
        Ok(Captcha::from_doc(&doc)?)
    }

    pub async fn get_challenge_bytes(&self, captcha: &Captcha) -> SolveResult<Bytes> {
        let res = self.client.get(captcha.challenge().as_str()).send().await?;
        let status = res.status();
        if !status.is_success() {
            return Err(SolveError::InvalidStatus(status));
        }
        Ok(res.bytes().await?)
    }

    pub async fn submit(
        &self,
        key: &CaptchaKey,
        captcha: &Captcha,
        answer: &str,
    ) -> SolveResult<()> {
        let url = {
            let form_data = captcha.get_form_data(key, answer);
            let res = self
                .client
                .post("http://api.solvemedia.com/papi/verify.noscript")
                .form(&form_data)
                .send()
                .await?;

            let status = res.status();
            if !status.is_success() {
                return Err(SolveError::InvalidStatus(status));
            }

            let doc = Document::from_read(res.bytes().await?.reader())?;

            let redirect = doc
                .find(Name("meta"))
                .last()
                .and_then(|el| el.attr("content"))
                .ok_or(SolveError::MissingRedirect)?
                .trim_start_matches("0; URL=");

            if redirect.starts_with("http://api.solvemedia.com/papi/challenge.noscript") {
                return Err(SolveError::InvalidResponse);
            }

            if !redirect.starts_with("http://api.solvemedia.com/papi/verify.pass.noscript") {
                return Err(SolveError::UnknownRedirectUrl(String::from(redirect)));
            }

            Url::parse(redirect).map_err(SolveError::InvalidRedirect)?
        };

        let res = self.client.get(url.as_str()).send().await?;
        let doc = Document::from_read(res.bytes().await?.reader())?;

        let result = doc
            .find(Name("textarea"))
            .last()
            .ok_or(SolveError::MissingCode)?
            .find(Text)
            .last()
            .and_then(|t| t.as_text())
            .ok_or(SolveError::MissingCode)?;

        if result != captcha.code() {
            return Err(SolveError::InvalidCode);
        }

        Ok(())
    }

    pub async fn solve<T: Fn(&Captcha, Bytes) -> F, F: Future<Output = SolveResult<String>>>(
        &self,
        key: &CaptchaKey,
        cb: T,
    ) -> SolveResult<Captcha> {
        let captcha = self.get_captcha(key).await?;
        let challenge_bytes = self.get_challenge_bytes(&captcha).await?;
        let answer = cb(&captcha, challenge_bytes).await?;
        self.submit(key, &captcha, &answer).await?;

        Ok(captcha)
    }
}
