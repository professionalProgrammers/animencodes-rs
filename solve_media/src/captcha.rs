use crate::CaptchaKey;
use select::{
    document::Document,
    predicate::{
        And,
        Attr,
        Name,
    },
};
use serde::Serialize;
use url::Url;

#[derive(Debug)]
pub enum CaptchaError {
    MissingCode,
    MissingChallenge,
    InvalidChallenge(url::ParseError),
}

#[derive(Debug, Clone)]
pub struct Captcha {
    code: String,
    challenge: Url,
    magic: String,
}

impl Captcha {
    pub(crate) fn from_doc(doc: &Document) -> Result<Self, CaptchaError> {
        let code = doc
            .find(And(Name("input"), Attr("name", "adcopy_challenge")))
            .last()
            .and_then(|el| el.attr("value"))
            .ok_or(CaptchaError::MissingCode)?
            .to_string();

        let challenge = doc
            .find(And(Name("img"), Attr("id", "adcopy-puzzle-image")))
            .last()
            .and_then(|el| el.attr("src"))
            .map(|path| {
                Url::parse("http://api.solvemedia.com")
                    .and_then(|u| u.join(path))
                    .map_err(CaptchaError::InvalidChallenge)
            })
            .ok_or(CaptchaError::MissingChallenge)??;

        let magic = doc
            .find(And(Name("input"), Attr("name", "magic")))
            .last()
            .and_then(|el| el.attr("value"))
            .ok_or(CaptchaError::MissingChallenge)?
            .to_string();

        Ok(Captcha {
            code,
            challenge,
            magic,
        })
    }

    pub fn code(&self) -> &str {
        &self.code
    }

    pub fn challenge(&self) -> &Url {
        &self.challenge
    }

    pub fn magic(&self) -> &str {
        &self.magic
    }

    pub(crate) fn get_form_data<'a>(
        &'a self,
        key: &'a CaptchaKey,
        response: &'a str,
    ) -> CaptchaFormData {
        CaptchaFormData {
            adcopy_response: response,
            key: key.as_str(),
            lang: "en",
            kind: "img",
            s: "standard",
            magic: self.magic(),
            adcopy_challenge: self.code(),
            reference: "",
        }
    }
}

#[derive(Debug, Serialize)]
pub struct CaptchaFormData<'a> {
    adcopy_response: &'a str,

    #[serde(rename = "k")]
    key: &'a str,

    #[serde(rename = "l")]
    lang: &'static str,

    #[serde(rename = "t")]
    kind: &'static str,

    s: &'static str,
    magic: &'a str,
    adcopy_challenge: &'a str,

    #[serde(rename = "ref")]
    reference: &'static str,
}
