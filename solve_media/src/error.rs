use crate::captcha::CaptchaError;
use std::any::Any;

pub type SolveResult<T> = Result<T, SolveError>;

#[derive(Debug)]
pub enum SolveError {
    Reqwest(reqwest::Error),
    InvalidStatus(reqwest::StatusCode),
    Io(std::io::Error),
    InvalidCaptcha(CaptchaError),
    UserError(Box<dyn Any + Send + Sync + 'static>),
    MissingRedirect,
    UnknownRedirectUrl(String),
    InvalidRedirect(url::ParseError),
    MissingCode,
    InvalidCode,
    InvalidResponse,
}

impl SolveError {
    pub fn make_user_error<T: Any + Send + Sync + 'static>(e: T) -> Self {
        Self::UserError(Box::new(e))
    }
}

impl From<reqwest::Error> for SolveError {
    fn from(e: reqwest::Error) -> Self {
        Self::Reqwest(e)
    }
}

impl From<std::io::Error> for SolveError {
    fn from(e: std::io::Error) -> Self {
        Self::Io(e)
    }
}

impl From<CaptchaError> for SolveError {
    fn from(e: CaptchaError) -> Self {
        SolveError::InvalidCaptcha(e)
    }
}
