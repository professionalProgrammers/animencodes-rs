use std::{
    convert::{
        TryFrom,
        TryInto,
    },
    str::FromStr,
};
use url::Url;

#[derive(Debug)]
pub enum CaptchaKeyUrlError {
    Url(url::ParseError),
    MissingKey,
    InvalidKey(CaptchaKeyError),
}

impl From<url::ParseError> for CaptchaKeyUrlError {
    fn from(e: url::ParseError) -> Self {
        Self::Url(e)
    }
}

impl From<CaptchaKeyError> for CaptchaKeyUrlError {
    fn from(e: CaptchaKeyError) -> Self {
        Self::InvalidKey(e)
    }
}

#[derive(Debug)]
pub enum CaptchaKeyError {}

#[derive(Debug, Eq, PartialEq, Clone, Hash)]
pub struct CaptchaKey(String);

impl CaptchaKey {
    pub fn from_url_str(url: &str) -> Result<Self, <Self as TryFrom<Url>>::Error> {
        Self::from_url(&url.parse::<Url>()?)
    }

    pub fn from_url(url: &Url) -> Result<Self, <Self as TryFrom<Url>>::Error> {
        url.try_into()
    }

    pub fn from_key(k: String) -> Result<Self, <Self as TryFrom<String>>::Error> {
        k.try_into()
    }

    pub fn as_str(&self) -> &str {
        &self.0
    }

    pub fn get_noscript_url(&self) -> Url {
        Url::parse_with_params(
            "http://api.solvemedia.com/papi/challenge.noscript",
            &[("k", &self.0)],
        )
        .unwrap()
    }
}

impl TryFrom<String> for CaptchaKey {
    type Error = CaptchaKeyError;

    fn try_from(s: String) -> Result<Self, Self::Error> {
        Ok(Self(s))
    }
}

impl<'a> TryFrom<&'a str> for CaptchaKey {
    type Error = <Self as TryFrom<String>>::Error;

    fn try_from(s: &'a str) -> Result<Self, Self::Error> {
        String::from(s).try_into()
    }
}

impl<'a> TryFrom<&'a Url> for CaptchaKey {
    type Error = CaptchaKeyUrlError;

    fn try_from(url: &'a Url) -> Result<Self, Self::Error> {
        Ok(url
            .query_pairs()
            .find(|(k, _)| k == "k")
            .map(|(_, v)| v)
            .as_deref()
            .ok_or(Self::Error::MissingKey)?
            .try_into()?)
    }
}

impl TryFrom<Url> for CaptchaKey {
    type Error = CaptchaKeyUrlError;

    fn try_from(url: Url) -> Result<Self, Self::Error> {
        (&url).try_into()
    }
}

impl FromStr for CaptchaKey {
    type Err = <Self as TryFrom<String>>::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        s.try_into()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn it_works() {
        let url =
            "http://api.solvemedia.com/papi/challenge.noscript?k=Pn6QzlWx5u.xPlsbCl8iNGiQF8kd7ulu";
        let key = CaptchaKey::from_url_str(url).unwrap();
        assert_eq!(key.as_str(), "Pn6QzlWx5u.xPlsbCl8iNGiQF8kd7ulu");
    }
}
